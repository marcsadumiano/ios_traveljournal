//
//  DatePickerTextField.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 14/03/2018.
//

import UIKit

class DatePickerTextField: UITextField {

    let datePicker = UIDatePicker()
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if(action == #selector(paste(_:)) || action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(select(_:)) ){
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let btnDone = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([btnDone], animated: false)
        
        inputAccessoryView = toolbar
        datePicker.datePickerMode = .date
        inputView = datePicker
        
    }
    
    func donePressed(){
        //format date
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let dateString = formatter.string(from: datePicker.date)
        
        //display date
        text = "\(dateString)"
        endEditing(true)
    }

}
