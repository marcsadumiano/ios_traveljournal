//
//  Section.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 19/03/2018.
//

import Foundation

struct Section {
    
    var country:String!
    var isExpanded:Bool!
    var journals = [Journal]()
    var expandImg:UIImage = UIImage(named: "expand")!
    var collapseImg:UIImage = UIImage(named: "collapse")!
    
    init(country: String){
        self.country = country
        self.isExpanded = false
    }
    
    func getImg() -> UIImage{
        if(isExpanded){
            return expandImg
        } else {
            return collapseImg
        }
    }
    
}
