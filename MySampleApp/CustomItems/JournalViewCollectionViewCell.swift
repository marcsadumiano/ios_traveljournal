//
//  JournalViewCollectionViewCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 21/03/2018.
//

import UIKit

class JournalViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgListIcon: UIImageView!
    @IBOutlet weak var lblListTitle: UILabel!
    @IBOutlet weak var imgStoryIcon: UIImageView!
    @IBOutlet weak var lblStoryTitle: UILabel!
    
    
   
}
