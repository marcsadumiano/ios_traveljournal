//
//  SelectCountryViewTableViewCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 22/03/2018.
//

import UIKit

class SelectCountryViewTableViewCell: UITableViewCell {

    //@IBOutlet weak var imgCountry: UIImageView!
    //@IBOutlet weak var lblCountry: UILabel!
    //@IBOutlet weak var checkboxOutlet: UIButton!
    
    let imgViewCountry: UIImageView = {
        let img = UIImageView(image: #imageLiteral(resourceName: "unknown"))
        img.translatesAutoresizingMaskIntoConstraints = false
        
        return img
    }()
    
    let btnCheckbox: UIButton = {
        let button = UIButton(type: UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "checkboxNotFilled"), for: UIControlState.normal)
        button.isEnabled = false
        //button.setBackgroundImage(#imageLiteral(resourceName: "checkboxNotFilled"), for: UIControlState.normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    let lblCountry_: UILabel = {
        let label = UILabel()
        label.text = ""
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
        
    }
    
    fileprivate func setUpViews(){
        
        var constraintsContainer = [NSLayoutConstraint]()
        let viewsDict: [String:Any] = [
            "imgViewCountry" : imgViewCountry,
            "country" : lblCountry_,
            "btnCheckbox" : btnCheckbox
        ]
        
        //add views
        contentView.addSubview(imgViewCountry)
        contentView.addSubview(lblCountry_)
        contentView.addSubview(btnCheckbox)
        
        //set constraints
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgViewCountry(44)]-8-[country]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[btnCheckbox(40)]-15-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[imgViewCountry(44)]-10.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[country]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-7-[btnCheckbox(40)]-7.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        NSLayoutConstraint.activate(constraintsContainer)
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
