//
//  MyJournalCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 11/04/2018.
//

import Foundation

class MyJournalCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let lblUsername: UILabel = {
        let label = UILabel()
        label.text = "SampleItem"
        label.font = label.font.withSize(8)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblTitle: UILabel = {
        let label = UILabel()
        label.text = "title"
        label.font = label.font.withSize(21)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblStartDate: UILabel = {
        let label = UILabel()
        label.text = "startDate"
        label.font = label.font.withSize(11)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let lblEndDate: UILabel = {
        let label = UILabel()
        label.text = "endDate"
        label.font = label.font.withSize(11)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let img: UIImageView = {
        let image = UIImageView(image: UIImage(named: "book2"))
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    func setUpViews() {
        //views
        addSubview(img)
        addSubview(lblUsername)
        addSubview(lblTitle)
        addSubview(lblStartDate)
        addSubview(lblEndDate)
        
        var constraintContainer = [NSLayoutConstraint]()
        let viewsDict : [String:Any] = [
            "img" : img,
            "title" : lblTitle,
            "username" : lblUsername,
            "startDate" : lblStartDate,
            "endDate" : lblEndDate
        ]
        
        //constraints
        //img
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[img]-18-[title]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-18-[img]-13.5-[username]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        //journalTitle
        //lblTitle.centerYAnchor.constraint(equalTo: img.centerYAnchor)
        constraintContainer.append(NSLayoutConstraint(item: lblTitle, attribute: .centerY, relatedBy: .equal, toItem: img, attribute: .centerY, multiplier: 1, constant: 0))
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[img]-8-[title]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        //username
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:[img]-[username]-10.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        let usernameAlignLeading = NSLayoutConstraint(item: lblUsername, attribute: .leading, relatedBy: .equal, toItem: img, attribute: .leading, multiplier: 1, constant: 0)
        constraintContainer.append(usernameAlignLeading)
        //start
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[startDate]-15-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-18-[startDate]-3-[endDate]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        //end
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[endDate]-15-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:[startDate]-3-[endDate]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        NSLayoutConstraint.activate(constraintContainer)
    }
    
}
