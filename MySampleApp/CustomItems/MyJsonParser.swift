//
//  MyJsonParser.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 17/03/2018.
//

import Foundation

class MyJsonParser {
    
    static func parseJSON(text: String) -> [Dictionary<String, AnyObject>]?{
        
        do{
            let data = text.data(using: .utf8)
            let jsonResult = try JSONSerialization.jsonObject(with: data!, options: [])
            return jsonResult as? [Dictionary<String, AnyObject>]
        } catch{
            print(error.localizedDescription)
        }
        
        return nil
        
    }
    
    static func parseJSON2(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
