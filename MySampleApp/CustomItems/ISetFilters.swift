//
//  ISetFilters.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 08/05/2018.
//

import Foundation

protocol ISetFilters {
    
    func setFilter(sortby: String, country: String)
    
}
