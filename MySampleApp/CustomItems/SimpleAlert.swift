//
//  SimpleAlert.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 05/04/2018.
//

import Foundation

class SimpleAlert{
    
    var title:String?
    var message:String?
    
    static func builder(title:String?, message: String, dismissTitle:String) -> UIAlertController{
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: dismissTitle, style: UIAlertActionStyle.cancel, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        return alert
    }
    
}
