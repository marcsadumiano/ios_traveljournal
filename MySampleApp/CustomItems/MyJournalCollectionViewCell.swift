//
//  MyJournalCollectionViewCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 11/04/2018.
//

import UIKit

class MyJournalCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let img: UIImageView = {
        let image = UIImageView(image: UIImage(named: "book2"))
        image.contentMode = UIViewContentMode.scaleAspectFit
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    let lblJournalTitle: UILabel = {
        let label = UILabel()
        label.text = "Sample title"
        label.font = label.font.withSize(14)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 4
        label.textAlignment = NSTextAlignment.center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setUpViews(){
        
        var constraintsContainer = [NSLayoutConstraint]()
        var viewsDict:[String:Any] = [
            "img" : img,
            "lblJournalTitle" : lblJournalTitle
        ]
        
        //views
        addSubview(img)
        addSubview(lblJournalTitle)
        
        //constraints
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[img]-0-[lblJournalTitle]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[img]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[lblJournalTitle]-0-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        NSLayoutConstraint.activate(constraintsContainer)
    }
    
}
