//
//  MainViewTableViewCellSection.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 27/03/2018.
//

import UIKit

class MainViewTableViewCellSection: UITableViewCell {

    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var btnExpandCollapseOutlet: UIButton!
    @IBAction func btnExpandCollapse(_ sender: UIButton) {
        print("expandCollapse pressed!!!")
    }
    
    var isExpanded:Bool = false {
        didSet{
            
            if(isExpanded){
                print("\(isExpanded)")
                btnExpandCollapseOutlet.setImage(UIImage(named: "expand"), for: .normal)
            } else {
                print("\(isExpanded)")
                btnExpandCollapseOutlet.setImage(UIImage(named: "collapse"), for: .normal)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
