//
//  MyJournalSectionCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 11/04/2018.
//

import Foundation

class MyJournalSectionCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = UIColor.darkGray
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let img: UIImageView = {
        let img = UIImageView(image: UIImage(named: "Philippines"))
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let lblCountry: UILabel = {
        let label = UILabel()
        label.text = "Country"
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let button: UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setImage(#imageLiteral(resourceName: "collapse"), for: UIControlState.normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var isExpanded:Bool = false {
        didSet{
            
            if(isExpanded){
                print("\(isExpanded)")
                button.setImage(UIImage(named: "expand"), for: .normal)
            } else {
                print("\(isExpanded)")
                button.setImage(UIImage(named: "collapse"), for: .normal)
            }
        }
    }
    
    func setUpViews(){
        
        var constraintsContainer = [NSLayoutConstraint]()
        let viewsDict: [String:Any] = [
            "img" : img,
            "country" : lblCountry,
            "button" : button
        ]
        
        //views
        contentView.addSubview(img)
        contentView.addSubview(lblCountry)
        contentView.addSubview(button)
        
        //constraints
        //img
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[img(35)]-8-[country]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|[img(29)]-0.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        //country
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[country]-1.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[img]-8-[country]-8-[button]", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        //button
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "V:|[button(30)]-0.5-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        constraintsContainer += NSLayoutConstraint.constraints(withVisualFormat: "H:[country]-8-[button(30)]-15-|", options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict)
        
        NSLayoutConstraint.activate(constraintsContainer)
    }
    
}
