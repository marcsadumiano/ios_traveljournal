//
//  CreateListViewTableViewCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 13/03/2018.
//

import UIKit

class CreateListViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var txtListitem: UITextField!
    @IBOutlet weak var checkboxOutlet: UIButton!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    @IBAction func btnCheckbox(_ sender: UIButton) {
        isDone = !isDone
    }
    
    var isDone:Bool = false {
        didSet{
            if(isDone){
                checkboxOutlet.setImage(UIImage(named: "checkboxFilled"), for: .normal)
            } else {
                checkboxOutlet.setImage(UIImage(named: "checkboxNotFilled"), for: .normal)
            }
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        print("checkbox initialized")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}

