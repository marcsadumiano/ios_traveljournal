//
//  MainViewTableViewCell.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 10/03/2018.
//

import UIKit

class MainViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellImg: UIImageView!
    
    @IBOutlet weak var cellTitle: UILabel!
    
    @IBOutlet weak var cellUsername: UILabel!
    
    @IBOutlet weak var cellStartDate: UILabel!
    
    @IBOutlet weak var cellEndDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

