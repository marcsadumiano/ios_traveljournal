//
//  ISelectCountry.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 08/05/2018.
//

import Foundation

protocol ISelectCountry {
    func setCountry(country: String)
}
