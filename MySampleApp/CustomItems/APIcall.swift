//
//  APIcall.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 24/04/2018.
//

import Foundation

class APIcall {
    
    static func list(_ method: String, id:String?, prepareBody: (() -> String?)?, completion: @escaping ([Dictionary<String, Any>]) -> Void){
        
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/list"
        
        if(methodNameValue == "PUT" || methodNameValue == "DELETE"){
            methodPath += "/\(id!)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            
            requestBody = prepareBody!()
            print("\n\n\(methodNameValue):\n\(requestBody!)")
            
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        print("\n\nPARAMETERS\n")
        if let parameters = parameters {
            for(index, row) in (parameters.enumerated()) {
                print("\(index): \(row)")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[self](task: AWSTask) -> AnyObject? in
           
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print("\n\n\(responseString!)")
            print(result.statusCode)
            
            if(methodNameValue == "PUT" || methodNameValue == "POST"){
                
                DispatchQueue.main.async {
                    if let listResult = MyJsonParser.parseJSON(text: responseString!){
                        completion(listResult)
                    }
                }
                
            }
            return nil
        })
    }//end list
    
    static func story(_ method: String, id:String?, prepareBody: (() -> String?)?, completion: @escaping (Dictionary<String, Any>) -> Void){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/story"
        
        if(methodNameValue == "PUT" || methodNameValue == "DELETE"){
            methodPath += "/\(id!)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
           
            requestBody = prepareBody!()
            print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        print("\n\nPARAMETERS\n")
        if let parameters = parameters{
            for(index, row) in (parameters.enumerated()) {
                print("\(index): \(row)")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[self](task: AWSTask) -> AnyObject? in
            
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            if(methodNameValue == "POST"){
                DispatchQueue.main.async {
                    //strongSelf.statusLabel.text = "\(result.statusCode)"
                    //strongSelf.txtDescription.text = responseString
                    //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                    
                    
                    if let storyResult = MyJsonParser.parseJSON2(text: responseString!){
                        completion(storyResult)
                    }
                    
                }
            } else if(methodNameValue == "PUT"){
                
            }
            
            return nil
        })
       
    }
    
    static func photo(_ method:String, id:String?, queryString:String?, prepareBody:(() -> String?)?, completion: @escaping ([Dictionary<String, Any>]) -> Void){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString = queryString
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/photo"
        
        if let photoId = id {
            methodPath += "/\(photoId)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            
            requestBody = prepareBody!()
            //print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        /*
        print("\n\nPARAMETERS\n")
        if let parameters = parameters{
            for(index, row) in (parameters.enumerated()) {
                print("\(index): \(row)")
            }
        }*/
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[self](task: AWSTask) -> AnyObject? in
            
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            //print(responseString!)
            print(result.statusCode)
            
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.txtDescription.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                
                
                if let photoResult = MyJsonParser.parseJSON(text: responseString!){
                    completion(photoResult)
                } else{
                    print("\nERROR PARSING JSON!!\n")
                }
                
            }
            
            
            return nil
        })
    }
    
    static func user(_ method:String, id:String?, queryString:String?, prepareBody:(()->String?)?, completion:@escaping (([Dictionary<String, Any>])->Void)){
        
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString = queryString
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/user"
        
        if let userId = id {
            methodPath += "/\(userId)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            
            requestBody = prepareBody!()
            //print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        /*
         print("\n\nPARAMETERS\n")
         if let parameters = parameters{
         for(index, row) in (parameters.enumerated()) {
         print("\(index): \(row)")
         }
         }*/
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[self](task: AWSTask) -> AnyObject? in
            
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            //print("\nset profile picture result: \(responseString!)")
            print(result.statusCode)
            
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.txtDescription.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                
                
                if let userResult = MyJsonParser.parseJSON(text: responseString!){
                    completion(userResult)
                } else{
                    print("\nERROR PARSING JSON!!\n")
                }
                
            }
            
            return nil
        })
        
    }
    
    static func journal(_ method: String, id:String?, queryString:String?, prepareBody: (() -> String?)?, completion: @escaping (([Dictionary<String, Any>]) -> Void)){
        
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString = queryString
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/journal"
        
        if let journalId = id {
            methodPath += "/\(journalId)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            
            requestBody = prepareBody!()
            //print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        /*
         print("\n\nPARAMETERS\n")
         if let parameters = parameters{
         for(index, row) in (parameters.enumerated()) {
         print("\(index): \(row)")
         }
         }*/
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[self](task: AWSTask) -> AnyObject? in
            
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print("\nget journals filtered result: \(responseString!)")
            print(result.statusCode)
            
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.txtDescription.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                
                
                if let userResult = MyJsonParser.parseJSON(text: responseString!){
                    completion(userResult)
                } else{
                    print("\nERROR PARSING JSON!!\n")
                }
                
            }
            
            return nil
        })
        
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}
