//
//  CreateStoryViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 15/03/2018.
//

import UIKit
import Photos

class CreateStoryViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UITextViewDelegate, UITextFieldDelegate {

    var photos = [Photo]()
    var unsavedPhotos = [Photo]()
    var user:User?
    var journal:Journal?
    var story:Story?
    var storyIndex:Int?
    var tempStory:Story?
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var datePicker: DatePickerTextField!
    @IBOutlet weak var txtStory: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var btnAddPhotosOutlet: UIButton!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var btnDeleteStoryOutlet: UIButton!
    
    @IBAction func btnDeleteStory(_ sender: UIButton) {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this story?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: {(action) in
            
            if let story = self.story{
                if (story.id != nil && story.id != ""){
                    APIcall.story("DELETE", id: story.id, prepareBody: nil, completion: {(result) in
                        print("\nDelete result: \(result)")
                        
                    })
                }
                self.journal!.stories.remove(at: story.index!)
                self.navigationController?.popViewController(animated: true)
            }
            
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAddPhotos(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
        
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        if let story = story { //existing story
            story.title = txtTitle.text!
            if(datePicker.text != "Date (optional)" && datePicker.text != ""){
                story.date = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.datePicker.date)
            }
            story.description = txtStory.text
            story.photos = photos
            
            saveStory("PUT")
        }
        else if let tempStory = tempStory {
            tempStory.title = txtTitle.text!
            if(datePicker.text != "Date (optional)" && datePicker.text != ""){
                tempStory.date = Calendar.current.dateComponents([.year, .month, .day], from: datePicker.datePicker.date)
            }
            tempStory.description = txtStory.text
            tempStory.photos = photos
            journal!.stories.append(tempStory)
            if journal!.id != nil { //save if the journal is stored in the database (i.e it has a journal id)
                print("\n\nsaving new story for an existing journal!!!")
                saveStory("POST")
            }
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("\nCreateStoryView loaded!\n")
        if story != nil {
            
            //load photos
            if(story!.photos.isEmpty){
                loadPhotos()
            }
            
            //pre-populate fields
            let calendar = Calendar.current
            navigationItem.title = "Story"
            lblDate.text = "Date"
            txtTitle.text = story!.title
            txtStory.textColor = UIColor.black
            txtStory.text = story!.description
            if let date = story!.date{
                print("date found")
                datePicker.text = story!.getDate(style: .long)
                datePicker.datePicker.date = calendar.date(from: date)!
            }
            
            //view only mode
            if let user = user, let journal = journal, user.id != journal.user.id {
                viewStory()
            }
        }
        else{
            tempStory = Story()
            btnDeleteStoryOutlet.isHidden = true
        }
        
        btnSaveOutlet.layer.cornerRadius = 5
        btnDeleteStoryOutlet.layer.cornerRadius = 5
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let story = story {
            //recalculate indices
            for (index, row) in story.photos.enumerated(){
                story.photos[index].index = index
            }
            photos = story.photos
        }
        else if let tempStory = tempStory {
            //recalculate indices
            for (index, row) in tempStory.photos.enumerated(){
                tempStory.photos[index].index = index
            }
            photos = tempStory.photos
        }
        photosCollectionView.reloadData()
        print("\nCreateStory viewWillAppear: \(photos.count)")
    }
    
    func viewStory(){
        
        txtTitle.isUserInteractionEnabled = false
        datePicker.isUserInteractionEnabled = false
        btnSaveOutlet.isHidden = true
        txtStory.isEditable = false
        btnAddPhotosOutlet.isHidden = true
        btnDeleteStoryOutlet.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return photos.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! CreateStoryViewCollectionViewCell
        cell.imageView.image = photos[indexPath.row].photo
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let story = story {
            performSegue(withIdentifier: "viewPhotoSegue", sender: story.photos[indexPath.row])
        } else if let tempStory = tempStory {
            performSegue(withIdentifier: "viewPhotoSegue", sender: tempStory.photos[indexPath.row])
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? ViewPhotoViewController {
            let photo = sender as! Photo
            print("\nprepare photoId = \(photo.id)")
            destinationViewController.photo = photo
            destinationViewController.user = self.journal?.user
            if let story = story {
                destinationViewController.story = story
                
            }
            else if let tempStory = tempStory {
                destinationViewController.story = tempStory
                
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let photo = Photo(image)
        if let story = story{
            photo.index = story.photos.count
            story.photos.append(photo)
        } else if let tempStory = tempStory {
            photo.index = tempStory.photos.count
            tempStory.photos.append(photo)
        }
        
        photos.append(photo)
        unsavedPhotos.append(photo)
        photosCollectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //placeholder text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Story..."){
            textView.textColor = UIColor.black
            textView.text = ""
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            textView.textColor = UIColor.lightGray
            textView.text = "Story..."
        }
        textView.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n"){
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func loadPhotos(){
        
        let query = "?storyId=\(story!.id)"
        APIcall.photo("GET", id: nil, queryString: query, prepareBody: nil, completion: {(resultJSON) in
            
            var storyIDs = [String]()
            
            for dictionary in resultJSON{
                
                let photoId = dictionary["id"] as! Int
                storyIDs.append(String(photoId))
            
            }
            
            for index in storyIDs {
                APIcall.photo("GET", id: index, queryString: nil, prepareBody: nil, completion: {(photoResult) in
                    //print("\n\nresult for get{id}: \(photoResult)")
                    let photoRecord = photoResult[0]
                    let base64String = photoRecord["image"] as! String
                    let imgData = Data(base64Encoded: base64String, options: [])
                    if let imgData = imgData{
                        if let image = UIImage(data: imgData) {
                            let photo = Photo(image)
                            photo.id = String(photoRecord["id"] as! Int)
                            self.photos.append(photo)
                            self.photosCollectionView.reloadData()
                            
                            if let story = self.story{
                                photo.index = story.photos.count
                                story.photos.append(photo)
                            } else if let tempStory = self.tempStory {
                                photo.index = tempStory.photos.count
                                tempStory.photos.append(photo)
                            }
                            
                        }
                    }
                })
            }
            
        })
    }
    
    func saveStory(_ method: String){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/story"
        
        if(methodNameValue == "PUT"){
            methodPath += "/\(story!.id)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
            dateFormatter.locale = Locale(identifier: "en_GB")
            let calendar = Calendar.current
            //let dobDate = calendar.date(from: user.dob!)
            //let dobString = dateFormatter.string(from: dobDate!)
            //print("\ndateString: \(dobString)")
            
            
            
            if(methodNameValue == "POST"){
                
                var dateString = ""
                if let dateComps = tempStory?.date{
                    let date = calendar.date(from: dateComps)
                    dateString = dateFormatter.string(from: date!)
                }
                
                requestBody = "{\n  " +
                    "\"journalId\":\"\(journal!.id!)\",\n  " +
                    "\"title\":\"\(tempStory!.title)\",\n  " +
                    "\"descriptionn\":\"\(tempStory!.description)\"" +
                    "\(dateString != nil ? ",\n  \"date\":\"\(dateString)\"\n  " : "\n")" + "}"
                
            }
 
            else if(methodNameValue == "PUT"){
                
                var dateString = ""
                if let dateComps = story?.date{
                    let date = calendar.date(from: dateComps)
                    dateString = dateFormatter.string(from: date!)
                }
                
                requestBody = "{\n  " +
                    "\"journalId\":\"\(journal!.id!)\",\n  " +
                    "\"title\":\"\(story!.title)\",\n  " +
                    "\"descriptionn\":\"\(story!.description)\"" +
                    "\(dateString != nil ? ",\n  \"date\":\"\(dateString)\"\n  " : "\n")" + "}"
                
            }
            
            print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        print("\n\nPARAMETERS\n")
        if let parameters = parameters{
            for(index, row) in (parameters.enumerated()) {
                print("\(index): \(row)")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            if(methodNameValue == "POST"){
                DispatchQueue.main.async {
                    //strongSelf.statusLabel.text = "\(result.statusCode)"
                    //strongSelf.txtDescription.text = responseString
                    //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                    
                    
                    if let createdStoryResult = MyJsonParser.parseJSON2(text: responseString!){
                        let storyId = createdStoryResult["insertId"] as! Int
                        strongSelf.tempStory!.id = String(storyId)
                    }
                    
                }
            } else if(methodNameValue == "PUT"){
                
                
                //save unsavedPhotos
                if(!(strongSelf.unsavedPhotos.isEmpty)){
                    for (index, row) in strongSelf.unsavedPhotos.enumerated(){
                        APIcall.photo("POST", id: nil, queryString: nil, prepareBody: {
                            var requestBody:String?
                            
                            let imgData = UIImageJPEGRepresentation(row.photo!, 0.25)
                            let base64String = imgData!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
                            
                            requestBody = "{\n  \"storyId\":\"\(strongSelf.story!.id)\",\n  \"image\":\"\(base64String)\"\n}"
                            
                            return requestBody
                        }, completion: {(resultJSON) in
                            print("photosave result: \(resultJSON)")
                            row.id = String(resultJSON[0]["insertId"] as! Int)
                        })
                    }
                }
            }
            
            return nil
        })
        
    }

}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}

