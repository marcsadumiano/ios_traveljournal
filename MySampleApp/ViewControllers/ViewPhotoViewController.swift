//
//  ViewPhotoViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 05/04/2018.
//

import UIKit

class ViewPhotoViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    var photo:Photo?
    var story:Story?
    var user:User?
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = photo!.photo
        tabBarController?.tabBar.isHidden = true
        
        if let user = user {
            if user.id != User.currentUser.id{
                viewOnly()
            }
        }else {
            viewOnly()//precaution if no user was passed in
        }
        
    }
    
    fileprivate func viewOnly(){
        btnDeleteOutlet.isHidden = true
    }
    
    func deletePhoto(){
        story!.photos.remove(at: (photo!.index)!)
        
        APIcall.photo("DELETE", id: photo!.id, queryString: nil, prepareBody: nil, completion: {(resultJSON) in
            print("\ndelete result: \(resultJSON)")
            
        })
        
        
    }
    
    @IBAction func deletePhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: {(action) in
            self.deletePhoto()
            self.navigationController?.popViewController(animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
