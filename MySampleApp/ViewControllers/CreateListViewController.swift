//
//  CreateListViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 13/03/2018.
//

import UIKit

class CreateListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var txtListTitle: UITextField!
    @IBOutlet weak var txtNewListItem: UITextField!
    @IBOutlet weak var listItemTableView: UITableView!
    var activeTextField: UITextField!
    var list:List?
    var tempoList:List?
    var listItems = [ListItem]()
    @IBOutlet var btnAddOutlet: UIButton!
    @IBOutlet var btnSaveOutlet: UIButton!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    var journal:Journal?
    var user:User?
    var indexOfNewItems = [Int]()
    
    @IBAction func btnDelete(_ sender: UIButton) {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this list?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: {(action) in
            
            if let list = self.list{
                if let id = list.id{
                    APIcall.list("DELETE", id: id, prepareBody: nil, completion: {(result) in
                        print("\nDelete result: \(result)")
                        
                    })
                }
                self.journal!.lists.remove(at: list.index!)
                self.navigationController?.popViewController(animated: true)
            }
            
            
            alert.dismiss(animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnAdd(_ sender: UIButton) {
        if(txtNewListItem.text != "" && txtNewListItem != nil){
            listItems.append(ListItem(item: txtNewListItem.text!))
            txtNewListItem.text = ""
            txtNewListItem.resignFirstResponder()
            listItemTableView.reloadData()
        } else {
            let alert = SimpleAlert.builder(title: nil, message: "You need to enter some data.", dismissTitle: "OK")
            self.present(alert, animated: true, completion: nil)
        }
    }

    
    @IBAction func btnSave(_ sender: Any) {
        //TO-DO: close page and save list. if txtNewListItem is not null, prompt user!!!!
        if(txtListTitle.text != ""){
            
            if let list = list { //existing list (and journal since a list was passed in)
                list.listItems = listItems
                
                if let listId = list.id{
                    saveList("PUT")
                }else{
                    saveList("POST")
                }
                
            } else { //new list
                if let journal = journal{ //make sure there is a journal object referenced
                    list = tempoList
                    list!.listItems = listItems
                    if journal.id != nil {
                        saveList("POST")
                    }
                    journal.lists.append(list!)
                    
                }
            }
            
            let alert = UIAlertController(title: "Saved!", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:{ (action) in
                    alert.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
            }))
            
            self.present(alert, animated: true, completion: nil)
            
        } else {
            //prompt user....
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(list != nil){
            
            navigationItem.title = "List"
            txtListTitle.text = list!.title!
            listItems = list!.listItems
            listItemTableView.allowsSelection = false
            if(journal!.user.id != user!.id){
                txtListTitle.isUserInteractionEnabled = false
                txtNewListItem.isHidden = true
                btnAddOutlet.isHidden = true
                btnSaveOutlet.isHidden = true
                btnDeleteOutlet.isHidden = true
                
            }
        } else {
            tempoList = List()
            btnDeleteOutlet.isHidden = true
        }
        
        btnSaveOutlet.layer.cornerRadius = 5
        btnDeleteOutlet.layer.cornerRadius = 5
        
        let notif:NotificationCenter = NotificationCenter.default
        notif.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notif.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(listItems.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CreateListViewTableViewCell
        cell.txtListitem.tag = indexPath.row + 1 //0 is default
        cell.txtListitem.text = listItems[indexPath.row].item
        if let isDone = listItems[indexPath.row].isDone {
            cell.isDone = isDone
        } else {
            cell.isDone = false
        }
        
        cell.checkboxOutlet.tag = indexPath.row
        cell.checkboxOutlet.addTarget(self, action: #selector(checkBoxPressed), for: UIControlEvents.touchUpInside)
        
        cell.btnDeleteOutlet.tag = indexPath.row
        cell.btnDeleteOutlet.addTarget(self, action: #selector(self.btnDeletePressed), for: UIControlEvents.touchUpInside)
        
        //if(journal!.user.id != user!.id){
        if let userID = user?.id, journal!.user.id != userID{
            print("\n\n\(journal!.id) - USER ID NOT MATCHED\n\n")
            cell.checkboxOutlet.isUserInteractionEnabled = false
            cell.btnDeleteOutlet.isHidden = true
            cell.txtListitem.isUserInteractionEnabled = false
        } else{
            print("\n\n\(journal!.id) - USER ID MATCHED\n\n")
        }
        return(cell)
        
    }
    
    func checkBoxPressed(sender: UIButton){
        let index = sender.tag
        let isDone = listItems[index].isDone
        
        listItems[index].isDone = !(isDone!)
    }
    
    func btnDeletePressed(sender: UIButton){
        
        let alert = UIAlertController(title: "Delete list item", message: "Are you sure you want to delete this list item?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action) in
            self.listItems.remove(at: sender.tag)
            self.listItemTableView.reloadData()
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: {(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    
    }
    
    /*
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let listItemCell = tableView.cellForRow(at: indexPath) as! CreateListViewTableViewCell
        if let isDone =  listItems[indexPath.row].isDone{
            if(isDone){
                listItemCell.checkboxOutlet.setImage(UIImage(named: "checkboxNotFilled"), for: .normal)
                listItems[indexPath.row].isDone = !isDone
            } else{
                listItemCell.checkboxOutlet.setImage(UIImage(named: "checkboxFilled"), for: .normal)
                listItems[indexPath.row].isDone = !isDone
            }
        }
    }*/
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField.tag != 0){
            listItems[textField.tag - 1].item = textField.text!
        }
        if(textField == txtListTitle){
            if let list = list{
                list.title = txtListTitle.text!
            } else if let list = tempoList{
                list.title = txtListTitle.text!
            }
            
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func keyboardWillHide(notification:Notification){
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {self.view.frame = CGRect(x:0, y:0, width: self.view.bounds.width, height: self.view.bounds.height)}, completion: nil)
    }
    
    func keyboardDidShow(notification:Notification){
        let info:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardY = self.view.frame.size.height - keyboardSize.height
        let editingTextFieldY:CGFloat! = self.activeTextField?.frame.origin.y
        
        if ( (editingTextFieldY > keyboardY - 50) && (self.view.frame.origin.y >= 0) ) {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {self.view.frame = CGRect(x:0, y: self.view.frame.origin.y - (editingTextFieldY! - (keyboardY - 50)), width: self.view.bounds.width, height: self.view.bounds.height)}, completion: nil)
        }
    }
    
    func saveList(_ method: String){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/list"
        
        if(methodNameValue == "PUT"){
            methodPath += "/\(list!.id!)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            var listId:String = ""
            if let id = list?.id {
                listId = id
            }
            requestBody = "{\n  \"listId\":\"\(listId)\",\n  \"items\":[\n   "
            for(index, row) in list!.listItems.enumerated() {
                
                var listItemId:String = ""
                if (row.id != nil && row.id != ""){
                    listItemId = row.id!
                }else{
                    indexOfNewItems.append(index)
                }
                
                let isDoneString:String = row.isDone! ? "1" : "0"
                
                
                if(index != list!.listItems.count - 1){
                    requestBody = "\(requestBody!){\"itemId\":\"\(listItemId)\", \"item\":\"\(row.item)\", \"isDone\":\"\(isDoneString)\"},\n   "
                } else {
                    requestBody = "\(requestBody!){\"itemId\":\"\(listItemId)\", \"item\":\"\(row.item)\", \"isDone\":\"\(isDoneString)\"}\n  "
                }
            }
            requestBody = "\(requestBody!)],\n  \"title\":\"\(list!.title!)\",\n  \"journalId\":\"\(journal!.id!)\"\n}"
            
            print("\n\n\(methodNameValue):\n\(requestBody!)")
            
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        print("\n\nPARAMETERS\n")
        for(index, row) in (parameters?.enumerated())! {
            print("\(index): \(row)")
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print("\n\n\(responseString!)")
            print(result.statusCode)
            
            if(methodNameValue == "PUT" || methodNameValue == "POST"){
                
                DispatchQueue.main.sync {
                    if let listPutResult = MyJsonParser.parseJSON(text: responseString!){
                        let lastIndex = listPutResult.count - 1
                        //asign IDs to new listItems (IDs taken from result of INSERT statement)
                        for index in strongSelf.indexOfNewItems {
                            
                            let newId = (listPutResult[index] as! [String:Any])["insertId"] as! Int
                            strongSelf.listItems[index].id = String(newId)
                            print("\n\nNEW ID for index \(index): \(newId)")
                        }
                        
                        if (strongSelf.list!.id == nil || strongSelf.list!.id == ""){
                            let newListId = (listPutResult[lastIndex] as! [String:Any])["insertId"] as! Int
                            strongSelf.list!.id = String(newListId)
                            print("\n\nNEW ID for list: \(newListId)")
                        }
                    }
                }
                
            }
            
            return nil
        })
        
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}

