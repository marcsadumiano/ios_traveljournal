//
//  SettingsTableViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 07/04/2018.
//

import UIKit
import AWSCore
import AWSCognito
import AWSAPIGateway
import AWSAuthCore
import AWSAuthUI
import AWSGoogleSignIn
import AWSFacebookSignIn
import AWSUserPoolsSignIn

class SettingsTableViewController: UITableViewController {
    
    var user:User?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        if indexPath.row == 3 {
            User.justLoggedOut = true
            let alert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: UIAlertControllerStyle.actionSheet)
            alert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: {(action) in
                self.performSegue(withIdentifier: "unwindToMainViewSegue", sender: self)
                self.navigationController?.popToRootViewController(animated: true)
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            print("\nLogout from new profile VC!!!")
        }else if indexPath.row == 2 {
            let alert = UIAlertController(title: "Deactivate Account", message: "Are you sure you want to delete your account?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alert.addAction(UIAlertAction(title: "Deactivate", style: UIAlertActionStyle.destructive, handler: {(action) in
                
                let user = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().currentUser()
                
                if let user = user {
                    
                    user.delete()
                    APIcall.user("DELETE", id: User.currentUser.id, queryString: nil, prepareBody: nil, completion: {(results) in
                        
                        print("\ndelete user result: \(results.description)")
                        
                    })
                    self.performSegue(withIdentifier: "unwindToMainViewSegue", sender: self)
                    self.navigationController?.popToRootViewController(animated: true)
                }else{
                    SimpleAlert.builder(title: "Error", message: "There was an error loading your account. Log in again", dismissTitle: "OK")
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
        } else if(indexPath.row == 1){
            performSegue(withIdentifier: "changePasswordSegue", sender: nil)
        }else if indexPath.row == 0{
            performSegue(withIdentifier: "editProfileSegue", sender: self.user)
        } else{
            print("\nno cell selected: row - \(indexPath.row)")
        }
        
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EditProfileTableViewController{
            let user = sender as! User
            destination.user = user
        }
    }
    

}
