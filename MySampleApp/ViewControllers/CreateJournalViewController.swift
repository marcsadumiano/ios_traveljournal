//
//  CreateJournalViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 10/03/2018.
//

import UIKit
import AWSAuthCore
import AWSAPIGateway

class CreateJournalViewController: UIViewController, UITextViewDelegate, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, ISelectCountry {
    
    var user:User?
    var journal: Journal?
    var tempoJournal: Journal?
    var lists = [List]()
    var stories = [Story]()
    let btnCancel = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var storyCollectionView: UICollectionView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var lblSelectedCountry: UILabel!
    @IBOutlet weak var dateStart: DatePickerTextField!
    @IBOutlet weak var dateEnd: DatePickerTextField!
    @IBOutlet weak var btnNewListOutlet: UIButton!
    @IBOutlet weak var btnNewStoryOutlet: UIButton!
    @IBOutlet weak var btnSelectCountry: UIButton!
    @IBOutlet weak var btnSelectCountryOutlet: UIButton!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    @IBOutlet weak var btnDeleteOutlet: UIButton!
    
    @IBOutlet weak var lblPrivacy: UILabel!
    
    @IBOutlet weak var switchPrivacyOutlet: UISwitch!
    
    var isPublic: Bool = true{
        didSet{
            
            if(isPublic){
                lblPrivacy.text = "Public"
                switchPrivacyOutlet.isOn = true
            }else{
                lblPrivacy.text = "Private"
                switchPrivacyOutlet.isOn = false
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if journal != nil{
            
            //pre-populate fields
            let calendar = Calendar.current
            
            self.txtTitle.text = journal!.title
            self.txtDescription.textColor = UIColor.black
            self.txtDescription.text = journal!.description
            self.dateStart.text = journal!.getStartDate(style: .long)
            self.dateStart.datePicker.date = calendar.date(from: journal!.start_date!)!
            self.dateEnd.text = journal!.getEndDate(style: .long)
            self.dateEnd.datePicker.date = calendar.date(from: journal!.end_date!)!
            self.btnSelectCountryOutlet.setTitle(journal!.country!, for: UIControlState.normal)
            self.isPublic = journal!.isPublic!
            
            if let user = user, user.id != journal!.user.id{
                journalViewOnly()
            }
            
            if(journal!.lists.isEmpty){
                loadLists()
            }
            if(journal!.stories.isEmpty){
                loadStories()
            }
            
        } else { //create new journal view
            navigationItem.leftBarButtonItem = btnCancel
            btnCancel.target = self
            btnCancel.action = #selector(btnCancelAction)
            user = User.currentUser
            tempoJournal = Journal(user: user!)
            self.btnDeleteOutlet.isHidden = true
        }
        
        btnSaveOutlet.layer.cornerRadius = 5
        btnDeleteOutlet.layer.cornerRadius = 5
        
    }
    
    @IBAction func switchPrivacy(_ sender: UISwitch) {
        isPublic = !isPublic
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        //To-DO
        
        if(btnSaveOutlet.titleLabel?.text == "Create"){
            tempoJournal!.title = txtTitle.text!
            if(txtDescription.text! == "Description(optional)"){
                tempoJournal!.description = ""
            } else {
                tempoJournal!.description = txtDescription.text
            }
            tempoJournal!.country = btnSelectCountryOutlet.titleLabel!.text! //needs input validation e.g text not 'Select>'
            tempoJournal!.isPublic = isPublic ? true : false
            tempoJournal!.start_date = Calendar.current.dateComponents([.year, .month, .day], from: dateStart.datePicker.date)
            tempoJournal!.end_date = Calendar.current.dateComponents([.year, .month, .day], from: dateEnd.datePicker.date)
            
            print("\ntempoJournal details\n")
            print("title: \(tempoJournal!.title!)")
            print("description: \(tempoJournal!.description)")
            print("country: \(tempoJournal!.country)")
            print("start: \(tempoJournal!.getStartDate(style: DateFormatter.Style.medium))")
            print("end: \(tempoJournal!.getEndDate(style: DateFormatter.Style.medium))")
            
        } else if(btnSaveOutlet.titleLabel?.text == "Save Changes"){
            journal!.title = txtTitle.text!
            if(txtDescription.text! == "Description(optional)"){
                journal!.description = ""
            } else {
                journal!.description = txtDescription.text
            }
            journal!.country = btnSelectCountryOutlet.titleLabel!.text! //needs input validation e.g text not 'Select>'
            journal!.isPublic = isPublic ? true : false
            journal!.start_date = Calendar.current.dateComponents([.year, .month, .day], from: dateStart.datePicker.date)
            journal!.end_date = Calendar.current.dateComponents([.year, .month, .day], from: dateEnd.datePicker.date)
            
            print("\njournal details\n")
            print("title: \(journal!.title!)")
            print("description: \(journal!.description)")
            print("country: \(journal!.country)")
            print("start: \(journal!.getStartDate(style: DateFormatter.Style.medium))")
            print("end: \(journal!.getEndDate(style: DateFormatter.Style.medium))")
        }
        
        
        let saveAlert = UIAlertController(title: "Save", message: "Are you sure you want to save this journal?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        saveAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action) in
            
            var alert: UIAlertController!
            
            if(self.btnSaveOutlet.titleLabel?.text == "Create"){
                self.saveJournal("POST")
                
                alert = SimpleAlert.builder(title: "Saved", message: "Journal has been saved successfully!", dismissTitle: "OK")
                self.btnCancelAction()
            }
            else if(self.btnSaveOutlet.titleLabel?.text == "Save Changes"){
                self.saveJournal("PUT")
                alert = SimpleAlert.builder(title: "Saved", message: "Journal has been updated!", dismissTitle: "OK")
                self.navigationController?.popViewController(animated: true)
            }
            self.present(alert, animated: true, completion: nil)
        }))
        saveAlert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(saveAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func btnDelete(_ sender: UIButton) {
        let alert = UIAlertController(title: "Delete", message: "Are you sure you want to delete this journal?", preferredStyle: UIAlertControllerStyle.actionSheet)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.destructive, handler: {(action) in
            self.saveJournal("DELETE")
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func btnCancelAction(){
        print("\n\nJournal Creation Cancelled!!!!\n\n")
        if (journal != nil || tempoJournal != nil) {
            journal = nil
            tempoJournal = Journal(user: User.currentUser)
            btnSaveOutlet.setTitle("Create", for: .normal)
        }
        txtTitle.text = ""
        txtDescription.textColor = UIColor.lightGray
        txtDescription.text = "Description(optional)"
        btnSelectCountryOutlet.setTitle("Select >", for: .normal)
        dateStart.text = ""
        dateEnd.text = ""
        lists.removeAll()
        listCollectionView.reloadData()
        stories.removeAll()
        storyCollectionView.reloadData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let tempoJournal = tempoJournal{
            
            self.lists = tempoJournal.lists
            self.stories = tempoJournal.stories
            self.listCollectionView.reloadData()
            self.storyCollectionView.reloadData()
            if(tempoJournal.id != nil && tempoJournal.id != ""){
                btnSaveOutlet.setTitle("Save Changes", for: .normal)
            }
        }
        
        else if let journal = journal{
            self.lists = journal.lists
            self.stories = journal.stories
            self.storyCollectionView.reloadData()
            self.listCollectionView.reloadData()
            btnSaveOutlet.setTitle("Save Changes", for: .normal)
        }
    }
    
    func journalViewOnly(){
        
        self.txtTitle.isUserInteractionEnabled = false
        self.txtDescription.isEditable = false
        self.dateStart.isUserInteractionEnabled = false
        self.dateEnd.isUserInteractionEnabled = false
        self.btnNewListOutlet.isHidden = true
        self.btnNewStoryOutlet.isHidden = true
        self.btnSelectCountryOutlet.isEnabled = false
        self.btnSelectCountryOutlet.setTitleColor(UIColor.black, for: .normal)
        self.btnSelectCountryOutlet.setTitle(journal!.country, for: .normal)
        self.btnSaveOutlet.isHidden = true
        self.btnDeleteOutlet.isHidden = true
        self.switchPrivacyOutlet.isHidden = true
        self.lblPrivacy.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewListSegue" {
            let destinationViewController = segue.destination as! CreateListViewController
            let list = sender as? List
            if(tempoJournal == nil){
                destinationViewController.journal = journal
            } else {
                destinationViewController.journal = tempoJournal
            }
            destinationViewController.user = user
            destinationViewController.list = list
        }
        
        else if segue.identifier == "viewStorySegue" {
            let destinationViewController = segue.destination as! CreateStoryViewController
            let story = sender as? Story
            if(tempoJournal == nil){
                destinationViewController.journal = journal
            } else {
                destinationViewController.journal = tempoJournal
            }
            destinationViewController.story = story
            
            destinationViewController.user = user
        }
        
        else if segue.identifier == "createListSegue"{
            let destinationViewController = segue.destination as! CreateListViewController
            if(tempoJournal == nil){
                destinationViewController.journal = journal
            } else {
                destinationViewController.journal = tempoJournal
            }
        }
        
        else if segue.identifier == "createStorySegue"{
            let destinationViewController = segue.destination as! CreateStoryViewController
            if(tempoJournal == nil){
                destinationViewController.journal = journal
            } else {
                destinationViewController.journal = tempoJournal
            }
        }
        
        else if segue.identifier == "selectCountrySegue"{
            if let destinationViewController = segue.destination as? SelectCountryViewController{
                destinationViewController.selectCountryProtocol = self
                if btnSelectCountryOutlet.titleLabel?.text != "Select >"{
                    destinationViewController.currentCountry = btnSelectCountryOutlet.titleLabel!.text!
                }
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView == self.listCollectionView{
            if let journal = journal{
                performSegue(withIdentifier: "viewListSegue", sender: journal.lists[indexPath.row])
            }else{
                performSegue(withIdentifier: "viewListSegue", sender: tempoJournal!.lists[indexPath.row])
            }
            
        } else {
            if let journal = journal {
                performSegue(withIdentifier: "viewStorySegue", sender: journal.stories[indexPath.row])
            } else {
                performSegue(withIdentifier: "viewStorySegue", sender: tempoJournal?.stories[indexPath.row])
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == self.listCollectionView){
            if let journal = journal {
                return journal.lists.count
            }
            else {
                return tempoJournal!.lists.count
            }
        } else {
            if let journal = journal {
                return journal.stories.count
            }
            else {
                return tempoJournal!.stories.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == self.listCollectionView){
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listCollectionViewCell", for: indexPath) as! JournalViewCollectionViewCell
            cell.imgListIcon.image = UIImage(named: "todo2")
            cell.lblListTitle.text = lists[indexPath.row].title!
            lists[indexPath.row].index = indexPath.row
            return(cell)
        } else {
            var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storyCollectionViewCell", for: indexPath) as! JournalViewCollectionViewCell
            cell.imgStoryIcon.image = UIImage(named: "story2")
            cell.lblStoryTitle.text = stories[indexPath.row].title
            stories[indexPath.row].index = indexPath.row
            return(cell)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.isEqual(txtTitle)){
            
            let length = (textField.text?.count)! + string.count - range.length
            
            return length <= 25
            
        }else {
            return true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if(text == "\n"){
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    //placeholder text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Description(optional)"){
            textView.textColor = UIColor.black
            textView.text = ""
        }
        textView.becomeFirstResponder()
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            textView.textColor = UIColor.lightGray
            textView.text = "Description(optional)"
        }
        textView.resignFirstResponder()
    }
    
    func setCountry(country: String) {
        btnSelectCountryOutlet.setTitle(country, for: UIControlState.normal)
    }
    
    func loadLists(){
        
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = HTTPMethodGet
        var requestBody: String?
        var methodPath = "/list/journal/" + journal!.id!
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.txtDescription.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                
                if let jsonResult = MyJsonParser.parseJSON(text: responseString!){
                    
                    var currentListId:Int?
                    var list:List?
                    
                    
                    for (index, row) in jsonResult.enumerated() {
                        
                        let dateFormatter = DateFormatter()
                        let calendar = Calendar.current
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                        dateFormatter.locale = Locale(identifier: "en_GB")
                        
                        let listId = row["id"] as! Int
                        if(currentListId == nil || currentListId != listId){
                            if(list != nil){
                                strongSelf.journal!.lists.append(list!)
                                strongSelf.lists.append(list!)
                                strongSelf.storyCollectionView.reloadData()
                                list = nil
                            }
                            currentListId = listId
                            let title = row["title"] as! String
                            list = List(id: String(listId), title: title)
                        }
                        
                        let listItemId = row["listItem_id"] as! Int
                        let description = row["description"] as! String
                        let item = ListItem(id: String(listItemId), item: description)
                        if let isDone = row["isDone"] as? String {
                            print("\n\n\n\(isDone)\n\n\n")
                            if(isDone == "1"){
                                item.isDone = true
                                print("\n\n\n\(isDone) TRUE!\n\n\n")
                            } else{
                                item.isDone = false
                                print("\n\n\n\(isDone) FALSE!\n\n\n")
                            }
                        }
                        
                        list!.addListItem(item: item)
                        
                        if(index == (jsonResult.count - 1) && list != nil){
                            strongSelf.journal!.lists.append(list!)
                            strongSelf.lists.append(list!)
                            strongSelf.listCollectionView.reloadData()
                            print("\(list!.id)")
                        }
                        
                    }
                } else {
                    //strongSelf.txtDescription.text = "error parsing json"
                    print("error123")
                }
                
            }
            
            return nil
        })
    }
    
    func loadStories(){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = HTTPMethodGet
        var requestBody: String?
        var methodPath = "/story/journal/" + journal!.id!
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.txtDescription.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                
                if let jsonResult = MyJsonParser.parseJSON(text: responseString!){
                    
                    for (index, row) in jsonResult.enumerated() {
                        
                        let dateFormatter = DateFormatter()
                        let calendar = Calendar.current
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                        dateFormatter.locale = Locale(identifier: "en_GB")
                        
                        let id = row["id"] as! Int
                        let title = row["title"] as! String
                        let description = row["description"] as! String
                        let story = Story(id: String(id), title: title, description: description)
                        if let date = row["date"] as? String, let dateObj = dateFormatter.date(from: date){
                            print("\n\nDATE PARSED!!\n\n")
                            let dateComp = calendar.dateComponents([.year, .month, .day], from: dateObj)
                            story.date = dateComp
                        } else {
                            print("\n\nERROR PARSING DATE!!!\n\n")
                        }
                        strongSelf.journal!.stories.append(story)
                        strongSelf.stories.append(story)
                        strongSelf.storyCollectionView.reloadData()
                    }
                } else {
                    //strongSelf.txtDescription.text = "error parsing json"
                    print("error123")
                }
                
            }
            
            return nil
        })
    }
    
    func saveJournal(_ method: String){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/journal"
        
        if(methodNameValue == "PUT" || methodNameValue == "DELETE"){
            methodPath += "/\(journal!.id!)"
        }
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "POST" || methodNameValue == "PUT"){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
            dateFormatter.locale = Locale(identifier: "en_GB")
            let calendar = Calendar.current
            //let dobDate = calendar.date(from: user.dob!)
            //let dobString = dateFormatter.string(from: dobDate!)
            //print("\ndateString: \(dobString)")
            
            
            if(methodNameValue == "POST"){
                
                let startDate = calendar.date(from: tempoJournal!.start_date!)!
                let startDateString = dateFormatter.string(from: startDate)
                let endDate = calendar.date(from: tempoJournal!.end_date!)!
                let endDateString = dateFormatter.string(from: endDate)
                let isPublic = tempoJournal!.isPublic!
                let isPublicString = isPublic ? "1" : "0"
                print("\nBEFORE SAVE\n")
                print("\(startDateString) - \(endDateString), isPublic: \(isPublicString)")
                print("\n\(User.currentUser.id), \(tempoJournal!.title!), \(tempoJournal!.country!), \(tempoJournal!.description!) ")
                
                requestBody = "{\n  " +
                    "\"userId\":\"\(User.currentUser.id)\",\n  " +
                    "\"title\":\"\(tempoJournal!.title!)\",\n  " +
                    "\"startDate\":\"\(startDateString)\",\n  " +
                    "\"endDate\":\"\(endDateString)\",\n  " +
                    "\"country\":\"\(tempoJournal!.country!)\",\n  " +
                    "\"isPublic\":\"\(isPublicString)\",\n  " +
                    "\"descriptionn\":\"\(tempoJournal!.description!)\"\n" +
                "}"
            } else if(methodNameValue == "PUT"){
                
                let startDate = calendar.date(from: journal!.start_date!)!
                let startDateString = dateFormatter.string(from: startDate)
                let endDate = calendar.date(from: journal!.end_date!)!
                let endDateString = dateFormatter.string(from: endDate)
                let isPublic = journal!.isPublic!
                let isPublicString = isPublic ? "1" : "0"
                print("\nBEFORE SAVE\n")
                print("\(startDateString) - \(endDateString), isPublic: \(isPublicString)")
                print("\n\(User.currentUser.id), \(journal!.title!), \(journal!.country!), \(journal!.description!) ")
                
                requestBody = "{\n  " +
                    "\"userId\":\"\(journal!.user.id)\",\n  " +
                    "\"title\":\"\(journal!.title!)\",\n  " +
                    "\"startDate\":\"\(startDateString)\",\n  " +
                    "\"endDate\":\"\(endDateString)\",\n  " +
                    "\"country\":\"\(journal!.country!)\",\n  " +
                    "\"isPublic\":\"\(isPublicString)\",\n  " +
                    "\"descriptionn\":\"\(journal!.description!)\"\n" +
                "}"
            }
            
            print("\n\n\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        print("\n\nPARAMETERS\n")
        if let parameters = parameters{
            for(index, row) in (parameters.enumerated()) {
                print("\(index): \(row)")
            }
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            if(methodNameValue == "POST"){
                DispatchQueue.main.async {
                    //strongSelf.statusLabel.text = "\(result.statusCode)"
                    //strongSelf.txtDescription.text = responseString
                    //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                    
                    var response = MyJsonParser.parseJSON2(text: responseString!)
                    if let jid = response!["insertId"] as? Int{
                        strongSelf.tempoJournal!.id = String(jid)
                        print("\n\ntempoJournalID: \(jid)\n")
                        strongSelf.btnSaveOutlet.setTitle("Save Changes", for: UIControlState.normal)
                        strongSelf.journal = strongSelf.tempoJournal
                    }
                    
                    //---save list and list items after saving journal
                    for list in strongSelf.tempoJournal!.lists{
                        let currentList = list
                        APIcall.list("POST", id: nil, prepareBody: {
                            var requestBody:String?
                            
                            requestBody = "{\n  \"listId\":\"\",\n  \"items\":[\n   "
                            for(index, row) in currentList.listItems.enumerated() {
                                
                                
                                let isDoneString:String = row.isDone! ? "1" : "0"
                                
                                
                                if(index != currentList.listItems.count - 1){
                                    requestBody = "\(requestBody!){\"itemId\":\"\", \"item\":\"\(row.item)\", \"isDone\":\"\(isDoneString)\"},\n   "
                                } else {
                                    requestBody = "\(requestBody!){\"itemId\":\"\", \"item\":\"\(row.item)\", \"isDone\":\"\(isDoneString)\"}\n  "
                                }
                            }
                            requestBody = "\(requestBody!)],\n  \"title\":\"\(currentList.title!)\",\n  \"journalId\":\"\(strongSelf.tempoJournal!.id!)\"\n}"
                            
                            print("\n\nREQUESTBODY FROM CREATEJOURNAL:\n\(requestBody!)")
                            
                            return requestBody
                        }, completion: {(resultJSON) in
                            print("\n\nSUCCESS new api call: \(resultJSON.count)\n")
                            let lastIndex = resultJSON.count - 1
                            
                            for (index, item) in currentList.listItems.enumerated(){
                                let newId = (resultJSON[index] as! [String:Any])["insertId"] as! Int
                                item.id = String(newId)
                                print("\n\nNEW ID for index \(index): \(newId)")
                            }
                            
                            let newListId = (resultJSON[lastIndex] as! [String:Any])["insertId"] as! Int
                            currentList.id = String(newListId)
                            print("\n\nNEW ID for list: \(newListId)")
                        })
                    }
                    //---end save list and list items
                    
                    //---save stories after saving journal
                    for story in strongSelf.tempoJournal!.stories{
                        let currentStory = story
                        APIcall.story("POST", id: nil, prepareBody: {
                            var requestBody:String?
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                            dateFormatter.locale = Locale(identifier: "en_GB")
                            let calendar = Calendar.current
                            
                            var dateString = ""
                            if let dateComps = currentStory.date{
                                let date = calendar.date(from: dateComps)
                                dateString = dateFormatter.string(from: date!)
                            }
                            
                            requestBody = "{\n  " +
                                "\"journalId\":\"\(strongSelf.journal!.id!)\",\n  " +
                                "\"title\":\"\(currentStory.title)\",\n  " +
                                "\"descriptionn\":\"\(currentStory.description)\"" +
                                "\(dateString != nil ? ",\n  \"date\":\"\(dateString)\"\n  " : "\n")" + "}"
                            
                            return requestBody
                            
                        }, completion: { (resultJSON) in
                            let storyId = resultJSON["insertId"] as! Int
                            currentStory.id = String(storyId)
                            print("\n\nNEW ID FOR STORY: \(storyId)")
                            //save photos after saving story
                            
                            for (index, row) in currentStory.photos.enumerated() {
                                if row.id != ""{
                                    APIcall.photo("POST", id: nil, queryString: nil, prepareBody: {
                                        var requestBody:String?
                                        
                                        let imgData = UIImageJPEGRepresentation(row.photo!, 0.25)
                                        let base64String = imgData!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
                                        
                                        requestBody = "{\n  \"storyId\":\"\(currentStory.id)\",\n  \"image\":\"\(base64String)\"\n}"
                                        
                                        return requestBody
                                    }, completion: {(resultJSON) in
                                        print("photosave result: \(resultJSON)")
                                        row.id = String(resultJSON[0]["insertId"] as! Int)//assign new photos their ID taken from the results of inserting to database
                                    })
                                }
                            }
                            
                        })
                    }
                    //---end save stories
                    User.justLoggedOut = true //to reload profile page
                    //strongSelf.btnCancelAction()
                    
                }
            } else if(methodNameValue == "PUT"){
                
            } else if(methodNameValue == "DELETE"){
                DispatchQueue.main.async {
                    
                    let alert = SimpleAlert.builder(title: "Deleted.", message: "Journal has been deleted.", dismissTitle: "Close")
                    let deleteSuccess = UIAlertController(title: "Deleted", message: "Journal has been deleted.", preferredStyle: UIAlertControllerStyle.alert)
                    deleteSuccess.addAction(UIAlertAction(title: "Close", style: UIAlertActionStyle.cancel, handler: {(action) in
                        let viewControllers = strongSelf.navigationController?.viewControllers
                        if let count = viewControllers?.count {
                            if count > 1 { //refresh home page
                                if let mainViewController = viewControllers![count - 2] as? MainViewController{
                                    mainViewController.sections.removeAll()
                                    mainViewController.journals.removeAll()
                                    mainViewController.sectionsDict.removeAll()
                                    mainViewController.invoke(query: nil)
                                    mainViewController.tblJournals.reloadData()
                                    strongSelf.navigationController?.popViewController(animated: true)
                                }else{
                                    strongSelf.btnCancelAction()
                                    User.justLoggedOut = true //to reload profile page
                                    strongSelf.navigationController?.popToRootViewController(animated: true)
                                }
                            }else{ //back to profile
                                //strongSelf.btnCancelAction()
                                strongSelf.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                        
                    }))
                    strongSelf.present(deleteSuccess, animated: true, completion: nil)
                    
                }
                
            }
            
            return nil
        })
    
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}



