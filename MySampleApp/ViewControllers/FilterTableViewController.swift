//
//  FilterTableViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 08/05/2018.
//

import UIKit

class FilterTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, ISelectCountry {
    
    @IBOutlet weak var txtCountry: UILabel!
    @IBOutlet weak var txtSortby: UITextField!
    var setFiltersProtocol:ISetFilters?
    
    //for preparesegue to this controller
    var sortby:String?
    var sortbycountry:String?
    
    let sortByList = ["Most Recent", "Oldest", "Countries A-Z", "Countries Z-A"]
    var countrySelected:String? = "All" {
        didSet{
            if countrySelected == " All"{
                countrySelected == "All"
            }
            txtCountry.text = countrySelected
        }
    }
    
    let picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //set default values
        if let sortby = sortby, let sortbycountry = sortbycountry{
            txtSortby.text = sortby
            txtCountry.text = sortbycountry
        } else{
            txtSortby.text = sortByList[0]
            txtCountry.text = "All"
        }
        
        
        setUpPicker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    fileprivate func setUpPicker(){
        
        picker.delegate = self
        picker.dataSource = self
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let btn = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        toolbar.setItems([btn], animated: false)
        txtSortby.inputView = picker
        txtSortby.inputAccessoryView = toolbar
    }
    
    
    func donePressed(){
        let index = picker.selectedRow(inComponent: 0)
        txtSortby.text = sortByList[index]
        txtSortby.resignFirstResponder()
    }
    
    func setCountry(country: String) {
        countrySelected = country
    }
    
    @IBAction func btnApply(_ sender: UIButton) {
        setFiltersProtocol?.setFilter(sortby: txtSortby.text!, country: txtCountry.text!)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            txtSortby.becomeFirstResponder()
        } else if indexPath.row == 1 {
            performSegue(withIdentifier: "selectCountrySegue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? SelectCountryViewController {
            destinationViewController.countries.append(" All")
            destinationViewController.selectCountryProtocol = self
            if let country = countrySelected{
                destinationViewController.currentCountry = country
            }
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return sortByList[row]
        
    }

}
