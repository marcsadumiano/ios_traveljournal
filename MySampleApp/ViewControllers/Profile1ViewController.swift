//
//  Profile1ViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 10/04/2018.
//

import UIKit

enum JournalView {
    case list
    case tiled
}

class Profile1ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    var initialHeightConstraint: CGFloat?
    @IBOutlet weak var viewForJournals: UIView!
    @IBOutlet weak var viewForProfileDetails: UIView!
    var tblJournals: UITableView?
    var cvJournals: UICollectionView?
    var user: User?
    var sections = [Section]()
    var sectionsDict = Dictionary<String, Section>()
    var journals = [Journal]()
    @IBOutlet weak var scrollView: UIScrollView!
    
    var refreshControl:UIRefreshControl!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblBio: UITextView!
    @IBOutlet weak var lblJournalsCount: UILabel!
    @IBOutlet weak var lblJournalsCountLabel: UILabel!
    @IBOutlet weak var lblCountriesCount: UILabel!
    @IBOutlet weak var lblCountriesCountLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var btnEditProfileOutlet: UIButton!
    @IBOutlet weak var btnChangeProfilePicOutlet: UIButton!
    
    var journalsCount = 0 {
        didSet{
            lblJournalsCount.text = String(journalsCount)
        }
    }
    var countriesCount = 0 {
        didSet{
            lblCountriesCount.text = String(countriesCount)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        self.scrollView.addSubview(refreshControl)
        
        setUpProfile()
        setUpViewForJournals(.list)
        //user details view layout
        setProfileDetailsLayout()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if(User.justLoggedOut) {
            setUpProfile()
        }
    }
    
    func refresh(sender: AnyObject){
        invoke()
        refreshControl.endRefreshing()
    }
    
    @IBAction func segmentViewType(_ sender: UISegmentedControl) {
        let selectedIndex = sender.selectedSegmentIndex
        //print("\n selected index: \(selectedIndex)")
        
        if selectedIndex == 0 {
            journalView = .list
        } else {
            journalView = .tiled
        }
    }
    
    var journalView: JournalView = JournalView.list {
        didSet{
            setUpViewForJournals(journalView)
        }
    }
    
    @IBAction func btnChangeProfilePic(_ sender: UIButton) {
        print("\nchanging profile picture...")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func settingsPressed(){
        performSegue(withIdentifier: "settingsSegue", sender: self.user)
    }
    
    func editProfilePressed(){
        performSegue(withIdentifier: "editProfileSegue", sender: self.user)
    }
    
    
    fileprivate func viewOnly(){
        btnChangeProfilePicOutlet.isHidden = true
        btnEditProfileOutlet.isHidden = true
        self.navigationItem.rightBarButtonItem = nil
    }
    
    fileprivate func setRightButton(){
        //let btn = UIBarButtonItem(image: #imageLiteral(resourceName: "IconSettings"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(settingsPressed))
        let btn = UIBarButtonItem(title: "Settings", style: UIBarButtonItemStyle.plain, target: self, action: #selector(settingsPressed))
        self.navigationItem.rightBarButtonItem = btn
    }
    
    fileprivate func setProfileDetailsLayout(){
        lblJournalsCount.translatesAutoresizingMaskIntoConstraints = false
        lblJournalsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        lblCountriesCount.translatesAutoresizingMaskIntoConstraints = false
        lblCountriesCountLabel.translatesAutoresizingMaskIntoConstraints = false
        btnEditProfileOutlet.translatesAutoresizingMaskIntoConstraints = false
        
        let x = viewForProfileDetails.bounds.width/4 // 1/4 of the viewForProfileDetails width
        let y = viewForProfileDetails.bounds.height/6 // 1/6... height
        
        lblJournalsCount.centerXAnchor.constraint(equalTo: viewForProfileDetails.centerXAnchor, constant: -(x)).isActive = true
        lblJournalsCount.centerYAnchor.constraint(equalTo: viewForProfileDetails.centerYAnchor, constant: -(y*2)).isActive = true
        
        lblJournalsCountLabel.centerXAnchor.constraint(equalTo: viewForProfileDetails.centerXAnchor, constant: -(x)).isActive = true
        lblJournalsCountLabel.centerYAnchor.constraint(equalTo: viewForProfileDetails.centerYAnchor, constant: -(y)).isActive = true
        
        lblCountriesCount.centerXAnchor.constraint(equalTo: viewForProfileDetails.centerXAnchor, constant: (x)).isActive = true
        lblCountriesCount.centerYAnchor.constraint(equalTo: viewForProfileDetails.centerYAnchor, constant: -(y*2)).isActive = true
        
        lblCountriesCountLabel.centerXAnchor.constraint(equalTo: viewForProfileDetails.centerXAnchor, constant: (x)).isActive = true
        lblCountriesCountLabel.centerYAnchor.constraint(equalTo: viewForProfileDetails.centerYAnchor, constant: -(y)).isActive = true
        
        btnEditProfileOutlet.leftAnchor.constraint(equalTo: viewForProfileDetails.leftAnchor).isActive = true
        btnEditProfileOutlet.rightAnchor.constraint(equalTo: viewForProfileDetails.rightAnchor).isActive = true
        btnEditProfileOutlet.centerXAnchor.constraint(equalTo: viewForProfileDetails.centerXAnchor, constant: 0).isActive = true
        btnEditProfileOutlet.centerYAnchor.constraint(equalTo: viewForProfileDetails.centerYAnchor, constant: (x)).isActive = true //offset by 25%
        btnEditProfileOutlet.layer.cornerRadius = 5
        
    }
    
    func setUpProfile(){
        
        setUpViewForJournals(JournalView.list)
        
        if(user == nil || (User.justLoggedOut && user?.id != User.currentUser.id) ){
            user = User.currentUser
        }
        
        if(User.justLoggedOut){
            self.user?.id = User.currentUser.id
        }
        
        User.justLoggedOut = false
        
        //check if the user passed in is the use currently logged in to this device
        if(User.currentUser.id != user!.id){
            viewOnly() //visiting profile
        }else{
            //edit profile button
            btnEditProfileOutlet.addTarget(self, action: #selector(editProfilePressed), for: UIControlEvents.touchUpInside)
            setRightButton()
        }
        
        journals.removeAll()
        sectionsDict.removeAll()
        
        if let tbl = tblJournals {
            journalsCount = 0
            countriesCount = 0
            tbl.reloadData()
        }
        
        if let cv = cvJournals {
            journalsCount = 0
            countriesCount = 0
            cv.reloadData()
        }
        
        guard let strongUser = user else{
            print("\n failed to set strongUser")
            return
        }
        //set profile picture
        if strongUser.profile_pic == nil{
            
            self.profilePic.image = UIImage(named: "unknown")
            
            APIcall.user("GET", id: strongUser.id, queryString: "?col=profile_pic", prepareBody: nil, completion: {(userPhoto) in
                
                let result = userPhoto[0]
                if let base64String = result["profile_pic"] as? String {
                    print("\nbase64String length remainder:\(base64String.count % 4)")
                    let imgData = Data(base64Encoded: base64String, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
                    if let imgData = imgData{
                        if let image = UIImage(data: imgData){
                            print("\n profile picture set")
                            self.profilePic.image = image
                            strongUser.profile_pic = image
                        } else{
                            print("\n failed to set profile picture")
                        }
                    }else {
                        print("\n failed to convert to data object")
                    }
                }
                
            })
        }else{
            self.profilePic.image = strongUser.profile_pic!
        }
        
        invoke()//load user's profile
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        profilePic.image = image
        user!.profile_pic = image
        let query = "?set=profilePic"
        //save selected photo
        APIcall.user("PUT", id: user!.id, queryString: query, prepareBody: {
            
            var requestBody:String?
            let imgData = UIImageJPEGRepresentation(image, 0.25)
            let base64String = imgData!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
            
            requestBody = "{\n \"image\":\"\(base64String)\"\n}"
            
            return requestBody
            
        }, completion: {(savePhotoResult) in
            
            //
            print("\n saveProfilePic:\(savePhotoResult.description)")
            
        })
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func setUpViewForJournals(_ view: JournalView){
        let frame = CGRect(x: 0, y: 0, width: viewForJournals.frame.width, height: viewForJournals.frame.height)
        
        if initialHeightConstraint == nil {
            initialHeightConstraint = viewHeightConstraint.constant
        } else {
            viewHeightConstraint.constant = initialHeightConstraint!
            print("\nviewHeightConstraint: \(viewHeightConstraint.constant)")
        }
        
        let tableView = UITableView(frame: frame)
        tblJournals = tableView
        let collectionView = UICollectionView(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
        cvJournals = collectionView
        
        if view == .list {
            
            tableView.separatorColor = UIColor.lightGray
            tableView.delegate = self
            tableView.dataSource = self
            tableView.register(MyJournalCell.self, forCellReuseIdentifier: "journalCell")
            tableView.register(MyJournalSectionCell.self, forCellReuseIdentifier: "journalSectionCell")
            //tableView.tableFooterView = UIView()
            tableView.isScrollEnabled = false
            viewForJournals.addSubview(tableView)
            
            tableView.translatesAutoresizingMaskIntoConstraints = true
            tableView.center = CGPoint(x: viewForJournals.bounds.midX, y: viewForJournals.bounds.midY)
            tableView.autoresizingMask = [UIViewAutoresizing.flexibleHeight]
            
            tableView.layoutIfNeeded()
            print("\nheight of tableview: \(tableView.contentSize.height)")
            //viewHeightConstraint.constant = tableView.contentSize.height + viewHeightConstraint.constant
            if(!sections.isEmpty){
                viewHeightConstraint.constant = viewHeightConstraint.constant + (tableView.rect(forSection: 0).height * 2) - 250
            }
            
        } else {
            
            let width = viewForJournals.bounds.width / 2 - (16 * 3)
            
            let layout = UICollectionViewFlowLayout()
            layout.minimumInteritemSpacing = 16
            layout.minimumLineSpacing = 16
            layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
            layout.itemSize = CGSize(width: width, height: UIImage(named: "journal")!.size.height + 50)
            
            collectionView.collectionViewLayout = layout
            collectionView.delegate = self
            collectionView.dataSource = self
            collectionView.register(MyJournalCollectionViewCell.self, forCellWithReuseIdentifier: "collectioViewCell")
            collectionView.backgroundColor = UIColor.white
            collectionView.translatesAutoresizingMaskIntoConstraints = false
            viewForJournals.addSubview(collectionView)
            
            collectionView.translatesAutoresizingMaskIntoConstraints = true
            collectionView.center = CGPoint(x: viewForJournals.bounds.midX, y: viewForJournals.bounds.midY)
            collectionView.autoresizingMask = [UIViewAutoresizing.flexibleHeight]
            
            collectionView.layoutIfNeeded()
            print("\nheight of collectionview: \(collectionView.contentSize.height)")
            //viewHeightConstraint.constant = collectionView.contentSize.height + viewHeightConstraint.constant
            if(!journals.isEmpty){
                viewHeightConstraint.constant = viewHeightConstraint.constant + 16 + (width * CGFloat(journals.count) / 2) - 200
            }
        }
    }
    
    //TABLEVIEW FUNCTIONS
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsDict.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sec = Array(sectionsDict.values)[section]
        
        if sec.isExpanded {
            return sec.journals.count
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sec = Array(sectionsDict.values)[indexPath.section]
        let journal = sec.journals[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "journalCell", for: indexPath) as! MyJournalCell
        cell.lblUsername.text = journal.user.username!
        cell.lblTitle.text = journal.title!
        cell.lblStartDate.text = journal.getStartDate(style: DateFormatter.Style.medium)
        cell.lblEndDate.text = journal.getEndDate(style: DateFormatter.Style.medium)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        let section_ = Array(sectionsDict.values)[section]
        
        let sectionCell = tableView.dequeueReusableCell(withIdentifier: "journalSectionCell") as! MyJournalSectionCell
        sectionCell.button.tag = section
        sectionCell.button.addTarget(self, action: #selector(toggleSection), for: UIControlEvents.touchUpInside)
        sectionCell.isExpanded = section_.isExpanded
        sectionCell.img.image = UIImage(named: section_.country)
        sectionCell.lblCountry.text = section_.country
        sectionCell.button.setImage(section_.getImg(), for: UIControlState.normal)
        return sectionCell.contentView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "viewMyJournal", sender: Array(sectionsDict.values)[indexPath.section].journals[indexPath.row])
    }
    
    func toggleSection(sender: UIButton){
        print("toggled")
        let section = sender.tag
        var indexPaths = [IndexPath]()
        let sec = Array(sectionsDict.values)[section]
        
        for row in sec.journals.indices {
            print("\(section) - \(row)")
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = sec.isExpanded
        sectionsDict[sec.country]?.isExpanded = !(isExpanded!)
        
        if(isExpanded!){
            tblJournals!.deleteRows(at: indexPaths, with: .fade)
        } else {
            tblJournals!.insertRows(at: indexPaths, with: .fade)
        }
        tblJournals!.reloadSections(IndexSet(integer: section), with: .fade)
        viewHeightConstraint.constant = initialHeightConstraint! + tblJournals!.contentSize.height - 270
        
    }
    
    //END TABLEVIEW FUNCTIONS
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewMyJournal"{
            let journal = sender as! Journal
            if let destination = segue.destination as? CreateJournalViewController{
                destination.user = User.currentUser
                destination.journal = journal
                destination.navigationItem.title = journal.title!
            }
        } else if segue.identifier == "settingsSegue"{
            let user = self.user
            if let destination = segue.destination as? SettingsTableViewController{
                destination.user = user
            }
        } else if segue.identifier == "editProfileSegue"{
            let user = sender as? User
            if let destination = segue.destination as? EditProfileTableViewController{
                destination.user = user
            }
        }
    }
    
    //COLLECTIONVIEW FUNCTIONS
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return journals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectioViewCell", for: indexPath) as! MyJournalCollectionViewCell
        cell.lblJournalTitle.text = journals[indexPath.row].title!
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        performSegue(withIdentifier: "viewMyJournal", sender: journals[indexPath.row])
        
    }
    //END COLLECTIONVIEW FUNCTIONS
    
    //user's profile details
    func invoke(){
        
        journalsCount = 0
        countriesCount = 0
        sectionsDict.removeAll()
        journals.removeAll()
        tblJournals?.reloadData()
        cvJournals?.reloadData()
        
        var query = "?userId=" + user!.id //query for journal API
        
        if user!.id != User.currentUser.id{
            query = "\(query)&isPublic=1"
        }
        
        APIcall.user("GET", id: user!.id, queryString: nil, prepareBody: nil, completion: {(results) in
            
            //print("\nuser load results:\(results.description)")
            guard let strongUser = self.user else {
                
                return
            }
            let result = results[0]
            
            if let firstName = result["first_name"] as? String, let lastName = result["last_name"] as? String, let username = result["username"] as? String {
                
                strongUser.first_name = firstName
                strongUser.last_name = lastName
                strongUser.username = username
                var usernameDisplay = "\(username)"
                
                if firstName != ""  || lastName != "" {
                    usernameDisplay = "\(usernameDisplay) ("
                    if firstName != "" {
                        usernameDisplay = "\(usernameDisplay)\(firstName)"
                        
                        if lastName != "" {
                            usernameDisplay = "\(usernameDisplay) " //add space
                        }
                    }
                    
                    if lastName != "" {
                        usernameDisplay = "\(usernameDisplay)\(lastName))"
                    }
                }
                self.lblUsername.text = usernameDisplay
                
            }
            
            /*
            if let email = result["email"] as? String {
                strongUser.email = email
                //self.lblEmail.text = email
            }*/
            
            if let bio = result["bio"] as? String {
                strongUser.bio = bio
                self.lblBio.text = bio
            }
            
        })
        
        APIcall.journal("GET", id: nil, queryString: query, prepareBody: nil, completion: {(journalsResult) in
            
            for (index, row) in journalsResult.enumerated() {
                
                let dateFormatter = DateFormatter()
                let calendar = Calendar.current
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                dateFormatter.locale = Locale(identifier: "en_GB")
                
                //user
                var id = row["user_id"] as! Int
                let first_name = row["first_name"] as! String
                let last_name = row["last_name"] as! String
                /*
                let dob = row["dob"] as! String
                var date = dateFormatter.date(from: dob)!
                let dobComp = calendar.dateComponents([.year, .month, .day], from: date)
                let email = row["email"] as! String
                */
                let username = row["username"] as! String
                //var user = User(id: String(id), first_name: first_name, last_name: last_name, dob: dobComp, username: username, email: email)
                var user = User(id: String(id), first_name: first_name, last_name: last_name, username: username)
                if let bio = row["bio"] as? String {
                    user.bio = bio
                }
                
                //journal
                id =  row["id"] as! Int
                let title = row["title"] as! String
                let description = row["description"] as! String
                
                let startDate = row["start_date"] as! String
                var date = dateFormatter.date(from: startDate)!
                let startDateComp = calendar.dateComponents([.year, .month, .day], from: date)
                
                let endDate = row["end_date"] as! String
                date = dateFormatter.date(from: endDate)!
                let endDateComp = calendar.dateComponents([.year, .month, .day], from: date)
                let country = row["country"] as! String
                let isPublicString = row["isPublic"] as! String
                let isPublic:Bool = isPublicString == "1" ? true : false
                
                let journal = Journal(id: String(id), title: title, description: description, start_date: startDateComp, end_date: endDateComp, country: country, isPublic: isPublic, user: user)
                
                //if section already exists, append the journal read
                if var tSec = self.sectionsDict[journal.country!]{
                    tSec.journals.append(journal)
                    self.sectionsDict[journal.country!] = tSec
                }else{ //if it doesn't exist, create the section and assign it to a dictionary item, appen journal to new section
                    var tSec = Section(country: journal.country!)
                    tSec.journals.append(journal)
                    self.sectionsDict[journal.country!] = tSec
                    self.countriesCount += 1
                }
                
                self.journals.append(journal)
                self.journalsCount += 1
                
            }
            
            self.tblJournals?.reloadData()
            self.cvJournals?.reloadData()
            
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}
