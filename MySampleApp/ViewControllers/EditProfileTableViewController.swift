//
//  EditProfileTableViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 11/05/2018.
//

import UIKit

class EditProfileTableViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {

    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtDOB: DatePickerTextField!
    @IBOutlet weak var txtBio: UITextView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    var oldPic:UIImage?
    @IBOutlet weak var btnChangePic1: UIButton!
    @IBOutlet weak var btnChangePic2: UIButton!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    
    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnSaveOutlet.layer.cornerRadius = 5
        
        self.tableView.allowsSelection = false
        
        btnChangePic1.addTarget(self, action: #selector(changeProfilePic), for: UIControlEvents.touchUpInside)
        btnChangePic2.addTarget(self, action: #selector(changeProfilePic), for: UIControlEvents.touchUpInside)

        if let user = user{
            
            if let img = user.profile_pic{
                imgProfilePicture.image = img
                oldPic = img
            }else{
                oldPic = #imageLiteral(resourceName: "unknown")
            }
            
            txtFirstName.text = user.first_name!
            txtLastName.text = user.last_name!
            txtBio.textColor = UIColor.black
            txtBio.text = user.bio!
            //txtDOB.text = user.getDOB(style: .long)
            //txtDOB.datePicker.date = Calendar.current.date(from: user.dob!)!
            
            //load email, dob
            
            APIcall.user("GET", id: user.id, queryString: nil, prepareBody: nil, completion: {(results) in
                
                let result = results[0]
                
                if let email = result["email"] as? String{
                    self.user?.email = email
                    self.txtEmail.text = email
                }
                
                let dateFormatter = DateFormatter()
                let calendar = Calendar.current
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                dateFormatter.locale = Locale(identifier: "en_GB")
                
                if let dobString = result["dob"] as? String, let date = dateFormatter.date(from: dobString){
                    //let date = dateFormatter.date(from: dobString)!
                    let dobComp = calendar.dateComponents([.year, .month, .day], from: date)
                    self.user!.dob = dobComp
                    self.txtDOB.text = self.user!.getDOB(style: .long)
                    self.txtDOB.datePicker.date = date
                }
                
                
            })
            
        }
        
        
    }
    
    func changeProfilePic(){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imgProfilePicture.image = image
        
        if let user = self.user{
            user.profile_pic = image
        }
        
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func btnSave(_ sender: UIButton) {
        print("\nuser details\nfname:\(txtFirstName.text!)\nlname:\(txtLastName.text!)\ndob:\(txtDOB.text!)\nbio:\(txtBio.text!)")
        
        if let oldPic = oldPic, let newPic = imgProfilePicture.image{
            if !(oldPic.isEqual(newPic)){
                //api save
                let query = "?set=profilePic"
                APIcall.user("PUT", id: user!.id, queryString: query, prepareBody: {
                    
                    var requestBody:String?
                    let imgData = UIImageJPEGRepresentation(newPic, 0.25)
                    let base64String = imgData!.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
                    
                    requestBody = "{\n \"image\":\"\(base64String)\"\n}"
                    
                    return requestBody
                    
                }, completion: {(savePhotoResult) in
                    
                    //
                    print("\n saveProfilePic:\(savePhotoResult.description)")
                    
                    
                })
            }
        }
        
        if txtFirstName.text! != "" && txtLastName.text! != "" && txtDOB.text! != "" {
            
            user!.first_name = txtFirstName.text!
            user!.last_name = txtLastName.text!
            user!.dob = Calendar.current.dateComponents([.year, .month, .day], from: txtDOB.datePicker.date)
            user!.bio = txtBio.text!
            //encoding emojis
            //user!.bio = String(data: txtBio.text!.data(using: String.Encoding.nonLossyASCII, allowLossyConversion: true)!, encoding: String.Encoding.utf8)!
            
            APIcall.user("PUT", id: user!.id, queryString: nil, prepareBody: {
                
                var requestBody:String?
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                dateFormatter.locale = Locale(identifier: "en_GB")
                //let calendar = Calendar.current
                //let dobDate = calendar.date(from: self.user!.dob!)
                let dobDate = self.txtDOB.datePicker.date
                let dobString = dateFormatter.string(from: dobDate)
                
                requestBody = "{\n  \"id\":\"\(self.user!.id)\",\n  \"first_name\":\"\(self.user!.first_name!)\",\n  \"last_name\":\"\(self.user!.last_name!)\",\n  \"username\":\"\(self.user!.username!)\",\n  \"email\":\"\(self.user!.email!)\",\n  \"bio\":\"\(self.user!.bio!)\",\n  \"dob\":\"\(dobString)\" \n}"
                
                return requestBody
                
            }, completion: {(results) in
                
                let alert = SimpleAlert.builder(title: "Saved!", message: "Profile changes has been saved", dismissTitle: "OK")
                
            })
        }
        
        User.justLoggedOut = true //just to reload the profile page to show changes (re run invoke())
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //placeholder text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Bio"){
            textView.textColor = UIColor.black
            textView.text = ""
        }
        textView.becomeFirstResponder()
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            textView.textColor = UIColor.lightGray
            textView.text = "Bio"
        }
        textView.resignFirstResponder()
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

}
