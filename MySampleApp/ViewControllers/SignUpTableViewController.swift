//
//  SignUpTableViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 13/05/2018.
//

import UIKit
import AWSUserPoolsSignIn

class SignUpTableViewController: UITableViewController {
    
    @IBAction func btnSignUp(_ sender: UIButton) {
        let pool = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool()
        
        var attributes = [AWSCognitoIdentityUserAttributeType]()
        
        attributes.append(AWSCognitoIdentityUserAttributeType(name: "email", value: "testSignUp@yahoo.com"))
        
        pool.signUp("testSignUp", password: "Marcs123", userAttributes: attributes, validationData: nil).continueWith { (task:AWSTask<AWSCognitoIdentityUserPoolSignUpResponse>) -> Any? in
            
            let comp = task.isCompleted
            
            return nil
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

      
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
