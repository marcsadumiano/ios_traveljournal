//
// MainViewController.swift
// MySampleApp
//
//
// Copyright 2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
//
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.
//
// Source code generated from template: aws-my-sample-app-ios-swift v0.19
//

import UIKit
import AWSCore
import AWSCognito
import AWSAPIGateway
import AWSAuthCore
import AWSAuthUI
import AWSGoogleSignIn
import AWSFacebookSignIn
import AWSUserPoolsSignIn

protocol SettingFilters{
    func setFilter()
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ISetFilters {
    
    var refreshControl: UIRefreshControl!
    @IBOutlet weak var tblJournals: UITableView!
    var sectionsDict = Dictionary<String, Section>()
    var sections = [Section]()
    var journalsResponse:String?
    var journals = [Journal]()
    var syncClient: AWSCognito!
    var dataset: AWSCognitoDataset!
    var user:User?
    var userID:String?
    var username:String?
    var email:String?
    
    var sortby:String?
    var sortbyCountry:String?
    var sortedKeys:[String]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //handleLogout()
        self.setupRightBarButtonItem()
        
        // Default theme settings.
        let height: CGFloat = 20
        let bounds = self.navigationController!.navigationBar.bounds
        self.navigationController!.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tblJournals.addSubview(refreshControl)
        presentSignInViewController()
        
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue){
        handleLogout()
        
    }
    
    func setFilter(sortby: String, country: String) {
        self.sortby = sortby
        self.sortbyCountry = country
        
        let query = "?sortby=\(sortby)&country=\(country)"
        
        invoke(query: query)
        
    }
    
    func refresh(sender: AnyObject){
        invoke(query: nil) //re-run api call to reload tableview
        refreshControl.endRefreshing()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionsDict.count
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var sec:Section!
        
        if let keys = sortedKeys {
            sec = sectionsDict[keys[section]]
        } else {
            sec = Array(sectionsDict.values)[section]
        }
        
        if sec.isExpanded {
            return sec.journals.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainViewTableViewCell
        var sec:Section!
        
        if let keys = sortedKeys{
            sec = sectionsDict[keys[indexPath.section]]
        }else{
            sec = Array(sectionsDict.values)[indexPath.section]
        }
        
        cell.cellTitle.text = sec.journals[indexPath.row].title
        cell.cellImg.image = UIImage(named: "book2")
        cell.cellUsername.text = sec.journals[indexPath.row].user.username
        cell.cellStartDate.text = sec.journals[indexPath.row].getStartDate(style: .short)
        cell.cellEndDate.text = sec.journals[indexPath.row].getEndDate(style: .short)
        
        return(cell)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cellSection = tableView.dequeueReusableCell(withIdentifier: "cellSection") as! MainViewTableViewCellSection
        
        var sec:Section!
        
        if let keys = sortedKeys {
            sec = sectionsDict[keys[section]]
        } else {
            sec = Array(sectionsDict.values)[section]
        }
        
        cellSection.lblCountry.text = sec.country
        cellSection.imgCountry.image = UIImage(named: sec.country)
        cellSection.btnExpandCollapseOutlet.tag = section
        cellSection.btnExpandCollapseOutlet.addTarget(self, action: #selector(toggleSection), for: UIControlEvents.touchUpInside)
        cellSection.btnExpandCollapseOutlet.setImage(sec.getImg(), for: .normal)
        cellSection.contentView.backgroundColor = UIColor.darkGray
        
        return cellSection.contentView
        
    }
    
    func toggleSection(sender: UIButton){
        print("toggled")
        let section = sender.tag
        var indexPaths = [IndexPath]()
        var sec:Section!
        
        if let keys = sortedKeys {
            sec = sectionsDict[keys[section]]
        } else {
            sec = Array(sectionsDict.values)[section]
        }
        
        for row in sec.journals.indices {
            print("\(section) - \(row)")
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = sec.isExpanded
        sectionsDict[sec.country]?.isExpanded = !(isExpanded!)
        
        if(isExpanded!){
            tblJournals.deleteRows(at: indexPaths, with: .fade)
        } else {
            tblJournals.insertRows(at: indexPaths, with: .fade)
        }
        tblJournals.reloadSections(IndexSet(integer: section), with: .fade)
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        var sec:Section!
        if let keys = sortedKeys{
            sec = sectionsDict[keys[indexPath.section]]
        } else {
            sec = Array(sectionsDict.values)[indexPath.section]
        }
        
        //performSegue(withIdentifier: "viewJournalSegue", sender: sections[indexPath.section].journals[indexPath.row])
        performSegue(withIdentifier: "viewJournalSegue", sender: sec.journals[indexPath.row])
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? CreateJournalViewController {
            let journal = sender as? Journal
            destinationViewController.journal = journal
            destinationViewController.navigationItem.title = journal!.user.username! + "'s journal"
            destinationViewController.user = user
        }
        
            /*
        else if let destinationViewController = segue.destination as? FirstTimeUserViewController {
            let user = sender as? User
            destinationViewController.user = user
        } */
        
        
        else if let destinationViewController = segue.destination as? FilterTableViewController {
            
            destinationViewController.setFiltersProtocol = self
            destinationViewController.sortby = sortby
            destinationViewController.sortbycountry = sortbyCountry
        }
    }
    
    //var demoFeatures: [DemoFeature] = []
    fileprivate let loginButton: UIBarButtonItem = UIBarButtonItem(title: nil, style: .done, target: nil, action: nil)
    
    // MARK: - View lifecycle
    
    func onSignIn (_ success: Bool) {
        // handle successful sign in
        if (success) {
            self.setupRightBarButtonItem()
        } else {
            // handle cancel operation from user
        }
    }
    
    
    func setupRightBarButtonItem() {
        //navigationItem.rightBarButtonItem = loginButton
        //navigationItem.rightBarButtonItem!.target = self
        //navigationItem.rightBarButtonItem!.image = UIImage(named: "filter")
        
        /*
        if (AWSSignInManager.sharedInstance().isLoggedIn) {
            navigationItem.rightBarButtonItem!.title = NSLocalizedString("Sign-Out", comment: "Label for the logout button.")
            navigationItem.title = "TravelJournal";
            navigationItem.rightBarButtonItem!.action = #selector(MainViewController.handleLogout)
        }*/
        
        let filterButton = UIButton(type: UIButtonType.system)
        filterButton.setImage(UIImage(named: "filter"), for: UIControlState.normal)
        filterButton.setTitle("Filter", for: UIControlState.normal)
        filterButton.addTarget(self, action: #selector(selectFilter(_:)), for: UIControlEvents.touchUpInside)
        filterButton.sizeToFit()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: filterButton)
        
    }
    
    func selectFilter(_ sender: UIBarButtonItem){
        self.performSegue(withIdentifier: "filterSegue", sender: nil)
    }
    
    func presentSignInViewController() {
        if !AWSSignInManager.sharedInstance().isLoggedIn {
            
            let config = AWSAuthUIConfiguration()
            config.enableUserPoolsUI = true
            //config.addSignInButtonView(class: AWSFacebookSignInButton.self)
            //config.addSignInButtonView(class: AWSGoogleSignInButton.self)
            config.logoImage = UIImage(named: "traveljournalLogo")
            config.canCancel = false
            
            AWSAuthUIViewController.presentViewController(with: self.navigationController!,configuration: config,
                completionHandler: { (provider: AWSSignInProvider, error: Error?) in
                if error != nil {
                    print("Error occurred: \(String(describing: error))")
                } else {
                    
                    self.username = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().currentUser()?.username
                    
                    let token = provider.token().result as? String
                    let arr = token!.split(separator: ".")
                    var base64String = String(arr[1])
                    if base64String.characters.count % 4 != 0 {
                        let padlen = 4 - base64String.characters.count % 4
                        base64String += String(repeating: "=", count: padlen)
                    }
                    
                    if let data = Data(base64Encoded: base64String, options: []), let str = String(data: data, encoding: String.Encoding.utf8){
                        let result = MyJsonParser.parseJSON2(text: str)
                        print("\n\(str)\n")
                        if let username = result!["cognito:username"] {
                            //self.username = username as! String
                            
                        }
                        if let email = result!["email"] {
                            self.email = email as! String
                            print("\n email read from aws: \(email as! String)")
                        }
                        
                    } else {
                        print("\n\nERROR decoding base64 string\n\n")
                    }
                    
                    print("\nTOKEN: \(base64String)\n")
                    
                    
                    self.syncClient = AWSCognito.default()
                    
                    self.dataset = self.syncClient.openOrCreateDataset("userDetails")
                    self.dataset.synchronize().continueWith {(task: AWSTask<AnyObject>) -> Any? in
                        if let error = task.error as NSError? {
                            print("\nLOAD SETTINGS ERROR: \(error.localizedDescription))\n")
                            return nil
                        }
                        if let value = self.dataset.string(forKey: "userID"), value != ""{
                            print("\nFOUND USERDETAILS \(value)\n")
                            self.userID = value
                            User.currentUser.id = value
                            self.loadUser("GET")
                            
                            
                        } else {
                            print("\nCREATING USER OBJECT\n")
                            self.loadUser("POST")
                            
                        }
                        
                        self.onSignIn(true)
                        self.invoke(query: nil)
                        return nil
                    }
                    
                }
            })
        } else { //if already signed in
            
            
            self.syncClient = AWSCognito.default()
            
            self.dataset = self.syncClient.openOrCreateDataset("userDetails")
            self.dataset.synchronize().continueWith {(task: AWSTask<AnyObject>) -> Any? in
                if let error = task.error as NSError? {
                    print("\nLOAD SETTINGS ERROR: \(error.localizedDescription))\n")
                    self.handleLogout()
                    return nil
                }
                let uid = self.dataset.string(forKey: "userID")
                self.userID = uid
                guard let id = uid else{
                    self.handleLogout()
                    return nil
                }
                
                User.currentUser.id = id
                
                self.username = self.dataset.string(forKey: "username")
                self.email = self.dataset.string(forKey: "email")
                print("\n\nalready signed in:\nuserID:\(self.userID)\nusername:\(self.username)\nemail:\(self.email)\n")
                self.loadUser("GET")
                
                self.invoke(query:nil)
                return nil
            }
        }
    }
    
    func handleLogout() {
        print("\nhandleLogout ran!")
        
        if (AWSSignInManager.sharedInstance().isLoggedIn) {
            
            AWSSignInManager.sharedInstance().logout(completionHandler: {(result: Any?, error: Error?) in
                self.sections.removeAll()
                self.sectionsDict.removeAll()
                self.tblJournals.reloadData()
                
                self.setupRightBarButtonItem()
                AWSCognitoUserPoolsSignInProvider.sharedInstance().reloadSession()
                let pool = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool()
                pool.currentUser()?.signOutAndClearLastKnownUser()
                self.syncClient.wipe()
                
                /*
                for(index, navController) in (self.tabBarController?.viewControllers?.enumerated())!{
                    //navController.navigationController?.popToRootViewController(animated: true)
                    //navController.presentedViewController?.navigationController?.popToRootViewController(animated: true)
                }*/
                User.currentUser.profile_pic = nil
                //AWSCognitoUserPoolsSignInProvider.sharedInstance().
                //self.navigationController!.popToRootViewController(animated: false)
                self.presentSignInViewController()
                
            })
            /*
            let cognito = AWSCognitoUserPoolsSignInProvider.sharedInstance()
            let pool = cognito.getUserPool()
            print("\n \(pool.currentUser()?.username)")
            pool.currentUser()?.signOut()
            cognito.logout()
            self.sections.removeAll()
            self.sectionsDict.removeAll()
            self.tblJournals.reloadData()
            self.setupRightBarButtonItem()
            User.currentUser.profile_pic = nil
            self.presentSignInViewController()*/
            
            // print("Logout Successful: \(signInProvider.getDisplayName)");
        } else {
            assert(false)
        }
    }
    
    func invoke(query:String?){
        
        var query = query
        //default. no query passed in
        if query == nil {
            sortby = "Most Recent"
            sortbyCountry = "All"
            query = "?sortby=Most Recent&country=All"
            setFilter(sortby: sortby!, country: sortbyCountry!)
        }
        
        sortedKeys = nil
        
        APIcall.journal("GET", id: nil, queryString: query, prepareBody: nil, completion: {(journalsResult) in
            
            self.sectionsDict.removeAll()
            
            for (index, row) in journalsResult.enumerated() {
                
                let dateFormatter = DateFormatter()
                let calendar = Calendar.current
                dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                dateFormatter.locale = Locale(identifier: "en_GB")
                
                //user
                var id = row["user_id"] as! Int
                let first_name = row["first_name"] as! String
                let last_name = row["last_name"] as! String
                /* // move user obj loading to profile (only id, username needed for this controller)
                let dob = row["dob"] as! String
                var date = dateFormatter.date(from: dob)!
                let dobComp = calendar.dateComponents([.year, .month, .day], from: date)*/
                let username = row["username"] as! String
                //let email = row["email"] as! String
                
                //var user = User(id: String(id), first_name: first_name, last_name: last_name, dob: dobComp, username: username, email: email)
                /*
                if let bio = row["bio"] as? String {
                    user.bio = bio
                }*/
                let user = User(id: String(id), first_name: first_name, last_name: last_name, username: username)
                
                //journal
                id =  row["id"] as! Int
                let title = row["title"] as! String
                let description = row["description"] as! String
                
                let startDate = row["start_date"] as! String
                var date = dateFormatter.date(from: startDate)!
                let startDateComp = calendar.dateComponents([.year, .month, .day], from: date)
                
                let endDate = row["end_date"] as! String
                date = dateFormatter.date(from: endDate)!
                let endDateComp = calendar.dateComponents([.year, .month, .day], from: date)
                let country = row["country"] as! String
                let isPublicString = row["isPublic"] as! String
                let isPublic:Bool = isPublicString == "1" ? true : false
                
                let journal = Journal(id: String(id), title: title, description: description, start_date: startDateComp, end_date: endDateComp, country: country, isPublic: isPublic, user: user)
                
                //if section already exists, append the journal read
                if var tSec = self.sectionsDict[journal.country!]{
                    tSec.journals.append(journal)
                    self.sectionsDict[journal.country!] = tSec
                }else{ //if it doesn't exist, create the section and assign it to a dictionary item, appen journal to new section
                    var tSec = Section(country: journal.country!)
                    tSec.journals.append(journal)
                    self.sectionsDict[journal.country!] = tSec
                }
                
            }
            
            if let sortby = self.sortby {
                if (sortby == "Countries A-Z" || sortby == "Most Recent"){
                    self.sortedKeys = Array(self.sectionsDict.keys).sorted(by: <)
                }else if(sortby == "Countries Z-A"){
                    self.sortedKeys = Array(self.sectionsDict.keys).sorted(by: >)
                }
            }
            self.tblJournals.reloadData()
        })
        
    }
    
    func loadUser(_ method: String){
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = method
        var requestBody: String?
        var methodPath = "/user"
        
        if(methodNameValue == "GET") {
            methodPath = "/user/" + userID!
            print("\nAPI CALL GET \(methodPath)\n")
        }
        
                
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        
        if(methodNameValue == "POST"){
            requestBody = "{\n  \"first_name\":\"\",\n  " +
            "\"last_name\":\"\",\n  " +
            "\"email\":\"\(self.email!)\",\n  " +
            "\"username\":\"\(self.username!)\"\n}"
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print("\n get user response: \(responseString!)")
            print(result.statusCode)
            
            if(methodNameValue == "GET"){
                DispatchQueue.main.async {
                    //strongSelf.statusLabel.text = "\(result.statusCode)"
                    //strongSelf.apiResponse.text = responseString
                    //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                    
                    if let jsonResult = MyJsonParser.parseJSON(text: responseString!){
                        print("\n\nUSER CALL = \(jsonResult.count)\n\n");
                        
                        //if there are no results stored in the database with this ID,
                        //  create a new data
                        if(jsonResult.count == 0){
                            strongSelf.loadUser("POST")
                            return
                        }
                        
                        /*
                        let dateFormatter = DateFormatter()
                        let calendar = Calendar.current
                        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
                        dateFormatter.locale = Locale(identifier: "en_GB")
                        */
                        let result = jsonResult[0]
                        let id = result["id"] as! Int
                        let firstName = result["first_name"] as! String
                        let lastName = result["last_name"] as! String
                        guard let username = result["username"] as? String, username != "" else {
                            //in case user details are missing, recreate it
                            strongSelf.user = User(String(id))
                            strongSelf.user?.username = strongSelf.username
                            strongSelf.user?.email = strongSelf.email
                            print("\n email from aws: \(strongSelf.email!)")
                            User.currentUser.id = String(id)
                            User.currentUser.username = strongSelf.username
                            User.currentUser.email = strongSelf.email
                            
                            print("\n\nFAILED GET USER, new user object \(strongSelf.user!.id)\n\n")
                            //strongSelf.performSegue(withIdentifier: "FirstTimeUserSegue", sender: strongSelf.user)
                            
                            APIcall.user("PUT", id: String(id), queryString: nil, prepareBody: {
                                
                                var requestBody:String?
                                
                                requestBody = "{\n  \"first_name\":\"\",\n  " +
                                    "\"last_name\":\"\",\n  " +
                                    "\"email\":\"\(strongSelf.email!)\",\n  " +
                                    "\"id\":\"\(id)\",\n  " +
                                    "\"dob\":\"\",\n  " +
                                    "\"bio\":\"\",\n  " +
                                    "\"username\":\"\(strongSelf.username!)\"\n}"
                                
                                return requestBody
                            }, completion: {(results) in
                                
                                //
                                
                            })
                            
                            return
                        }
                        /*
                        let email = result["email"] as! String
                        if let bio = result["bio"] as? String {
                            User.currentUser.bio = bio
                        }
                        let dobString = result["dob"] as! String
                        let dobDate = dateFormatter.date(from: dobString)!
                        let dobComp = calendar.dateComponents([.year, .month, .day], from: dobDate) */
                        //strongSelf.user = User(id: String(id), first_name: firstName, last_name: lastName, dob: dobComp, username: username, email: email)
                        strongSelf.user = User(id: String(id), first_name: firstName, last_name: lastName, username: username)
                        User.currentUser.id = String(id)
                        User.currentUser.first_name = firstName
                        User.currentUser.last_name = lastName
                        //User.currentUser.dob = dobComp
                        User.currentUser.username = username
                        //User.currentUser.email = email
                        
                    } else {
                        //strongSelf.txtDescription.text = "error parsing json"
                        print("GET USER error parsing json")
                    }
                    
                }
            }
            else if(methodNameValue == "POST"){
                DispatchQueue.main.async {
                    //strongSelf.statusLabel.text = "\(result.statusCode)"
                    //strongSelf.apiResponse.text = responseString
                    //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                    
                    if let jsonResult = MyJsonParser.parseJSON(text: responseString!){
                        
                        let result = jsonResult[0]
                        let id = result["id"] as! Int
                        strongSelf.user = User(String(id))
                        strongSelf.user?.username = strongSelf.username
                        strongSelf.user?.email = strongSelf.email
                        User.currentUser.id = String(id)
                        print("\n\n\nPOST USER CREATED \(strongSelf.user!.id)\n\n")
                        //strongSelf.performSegue(withIdentifier: "FirstTimeUserSegue", sender: strongSelf.user)
                        //used username and email from dataset and created user in POST call section
                        strongSelf.syncClient = AWSCognito.default()
                        strongSelf.dataset = strongSelf.syncClient.openOrCreateDataset("userDetails")
                        strongSelf.dataset.setString(strongSelf.user!.id, forKey: "userID")
                        strongSelf.dataset.setString(strongSelf.user!.username!, forKey: "username")
                        strongSelf.dataset.setString(strongSelf.user!.email!, forKey: "email")
                        
                        strongSelf.dataset.synchronize().continueWith{(task: AWSTask<AnyObject>) -> Any? in
                            if let error = task.error as NSError? {
                                print("\nloadSettings error: \(error.localizedDescription)\n")
                                return nil
                            }
                            
                            return nil
                        }
                        
                        
                    } else {
                        //strongSelf.txtDescription.text = "error parsing json"
                        print("\n\n\nerror parsing json from userHandler - POST\n\n")
                    }
                }
            }
            
            return nil
        })
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}


class FeatureDescriptionViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "Back", style: .plain, target: nil, action: nil)
    }
}


