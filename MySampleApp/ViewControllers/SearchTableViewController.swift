//
//  SearchTableViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 10/05/2018.
//

import UIKit

class SearchTableViewController: UITableViewController, UISearchResultsUpdating{
    
    var searchController:UISearchController!
    var resultsController = UITableViewController()
    var users = ["mgsadumiano", "michael", "user123"] //test data
    var filteredUsers = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.resultsController.tableView.delegate = self
        self.resultsController.tableView.dataSource = self
        self.searchController.searchResultsUpdater = self
        
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filteredUsers.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        let fname = filteredUsers[indexPath.row].first_name!
        let lname = filteredUsers[indexPath.row].last_name!
        let uname = filteredUsers[indexPath.row].username!
        var usernameDisplay = "\(uname)"
        
        if fname != ""  || lname != "" {
            usernameDisplay = "\(usernameDisplay) ("
            if fname != "" {
                usernameDisplay = "\(usernameDisplay)\(fname)"
                
                if lname != "" {
                    usernameDisplay = "\(usernameDisplay) " //add space character
                }
            }
            
            if lname != "" {
                usernameDisplay = "\(usernameDisplay)\(lname))"
            }
        }

        cell.textLabel?.text = "\(usernameDisplay)"
        
        return cell
    }
    
    // MARK: - Table view delegates
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.searchController.isActive = false
        performSegue(withIdentifier: "viewProfileSegue", sender: filteredUsers[indexPath.row])
    }
    
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? Profile1ViewController{
            if let user = sender as? User{
                destinationViewController.user = user
                print("\n viewProfileSegue")
            }
        }
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if searchController.searchBar.text!.count > 2{
            filteredUsers.removeAll()
            searchUser(user: searchController.searchBar.text!)
        }
    }
    
    fileprivate func searchUser(user:String){
        
        var query = "?name=\(user)"
        
        APIcall.user("GET", id: nil, queryString: query, prepareBody: nil, completion: {(usersResult) in
            
            print("results: \(usersResult.description)")
            self.filteredUsers.removeAll()
            for (index, user) in usersResult.enumerated(){
                
                if let id = user["id"] as? Int, let username = user["username"] as? String, let firstName = user["first_name"] as? String, let lastName = user["last_name"] as? String{
                    
                    let user = User(id: String(id), first_name: firstName, last_name: lastName, username: username)
                    self.filteredUsers.append(user)
                }else{
                    print("\nerror parsing user")
                }
                
            }
            self.resultsController.tableView.reloadData()
            
        })
    }

}
