//
//  SelectCountryViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 22/03/2018.
//

import UIKit


class SelectCountryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating {
    
    @IBOutlet weak var tblCountries: UITableView!
    var searchController:UISearchController!
    var resultsController = UITableViewController()

    var countries = [String]()
    var filteredCountries = [String]()
    
    
    var selectCountryProtocol:ISelectCountry?
    var currentCountry:String?
    @IBOutlet weak var countryTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadCountries()
        setUpSearch()
        
        // Do any additional setup after loading the view.
    }
    
    func loadCountries(){
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            var country = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "country not found for code: \(code)"
            country = country.replacingOccurrences(of: "&", with: "and")
            
            countries.append(country)
            countries.sort()
        }
    }

    fileprivate func setUpSearch(){
        
        self.searchController = UISearchController(searchResultsController: self.resultsController)
        self.tblCountries.tableHeaderView = self.searchController.searchBar
        
        self.resultsController.tableView.delegate = self
        self.resultsController.tableView.dataSource = self
        self.tblCountries.register(SelectCountryViewTableViewCell.self, forCellReuseIdentifier: "Scell")
        self.resultsController.tableView.register(SelectCountryViewTableViewCell.self, forCellReuseIdentifier: "Scell")
        
        self.searchController.searchResultsUpdater = self
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        filteredCountries.removeAll()
        filteredCountries = countries.filter({(country) in
            
            if country.contains(searchController.searchBar.text!) {
                return true
            }else {
                return false
            }
            
            
        })
        self.resultsController.tableView.reloadData()
        //tblCountries.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filteredCountries.count == 0{
            return countries.count
        }else {
            return filteredCountries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Scell", for: indexPath) as! SelectCountryViewTableViewCell
        
        var country:String!
        
        if filteredCountries.count != 0 {
            country = filteredCountries[indexPath.row]
        }else {
            country = countries[indexPath.row]
        }
        
        //cell.lblCountry.text = country
        cell.lblCountry_.text = country
        if let countryImg = UIImage(named: country){
            //cell.imgCountry.image = countryImg
            cell.imgViewCountry.image = countryImg
        }else{
            //cell.imgCountry.image = #imageLiteral(resourceName: "unknown")
            cell.imgViewCountry.image = #imageLiteral(resourceName: "unknown")
        }
        if let countrySelected = currentCountry {
            if countrySelected == country{
                //cell.checkboxOutlet.setImage(#imageLiteral(resourceName: "checkboxFilled"), for: UIControlState.normal)
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "checkboxFilled"), for: UIControlState.normal)
            }else{
                //cell.checkboxOutlet.setImage(#imageLiteral(resourceName: "checkboxNotFilled"), for: UIControlState.normal)
                cell.btnCheckbox.setImage(#imageLiteral(resourceName: "checkboxNotFilled"), for: UIControlState.normal)
            }
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //currentSelection = tableView.cellForRow(at: indexPath) as! SelectCountryViewTableViewCell
        tableView.deselectRow(at: indexPath, animated: true)
        
        //let cell = tableView.cellForRow(at: indexPath) as! SelectCountryViewTableViewCell
        
        var country:String!
        if filteredCountries.count != 0 {
            country = filteredCountries[indexPath.row]
        }else{
            country = countries[indexPath.row]
        }
        self.searchController.isActive = false
        selectCountryProtocol?.setCountry(country: country)
        self.navigationController?.popViewController(animated: true)
    }

}
