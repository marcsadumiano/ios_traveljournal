//
//  ProfileViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 12/03/2018.
//

import UIKit

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tblJournals: UITableView!
    var num = ["1","2","3","4","5","6","7","8","9","10","11","12"]
    var countries = [String]()
    var user = User.currentUser
    
    @IBOutlet weak var txtName: UILabel!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return(countries.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel!.text = countries[indexPath.row]
        
        return cell
    }
    
    
    override func viewDidLoad() {
        //super.viewDidLoad()
        
        
        
        for code in NSLocale.isoCountryCodes as [String] {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "country not found for code: \(code)"
            var country = name.replacingOccurrences(of: "&", with: "and")
            
            countries.append(country)
        }
        countries.sort()
        tblJournals.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let user = User.currentUser
        txtName.text = "\(user.first_name) \(user.last_name)(\(user.username!))"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}

