//
//  FirstTimeUserViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 03/04/2018.
//

import UIKit
import AWSAuthCore
import AWSAPIGateway

class FirstTimeUserViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    var user: User?
    @IBOutlet weak var txtFirstname: UITextField!
    @IBOutlet weak var txtSurname: UITextField!
    @IBOutlet weak var txtDOB: DatePickerTextField!
    @IBOutlet weak var txtBio: UITextView!
    @IBOutlet weak var btnSaveOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setHideKeyboard()
        btnSaveOutlet.layer.cornerRadius = 5
        
        print("\nFIRSTTIMEUSER VIEW LOADED!\n")
        navigationItem.setHidesBackButton(true, animated: true)
        let welcomeAlert = UIAlertController(title: "Welcome", message: "Before using TravelJournal, you need to enter your personal details.", preferredStyle: UIAlertControllerStyle.alert)
        welcomeAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action) in
            welcomeAlert.dismiss(animated: true, completion: nil)
        }))
        self.present(welcomeAlert, animated: true, completion: nil)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //placeholder text
    func textViewDidBeginEditing(_ textView: UITextView) {
        if(textView.text == "Bio"){
            textView.textColor = UIColor.black
            textView.text = ""
        }
        textView.becomeFirstResponder()
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if(textView.text == ""){
            textView.textColor = UIColor.lightGray
            textView.text = "Bio"
        }
        textView.resignFirstResponder()
    }

    @IBAction func btnSaveAction(_ sender: UIButton) {
        
        guard let strongUser = user else {
            print("\nError loading user\n")
            return
        }
        
        if(txtFirstname.text == "" && txtSurname.text == "" && txtDOB.text == ""){
            let warning = UIAlertController(title: "Missing Input", message: "You need to enter data for the required fields!", preferredStyle: UIAlertControllerStyle.alert)
            warning.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: {(action) in
                warning.dismiss(animated: true, completion: nil)
            }))
            self.present(warning, animated: true, completion: nil)
            return
        }
        
        strongUser.first_name = txtFirstname.text!
        strongUser.last_name = txtSurname.text!
        if txtBio.text! != ""{
            strongUser.bio = txtBio.text!
        }else{
            strongUser.bio = ""
        }
        
        let dobComp = Calendar.current.dateComponents([.year, .month, .day], from: txtDOB.datePicker.date)
        strongUser.dob = dobComp
        User.currentUser = strongUser
        saveUser(strongUser)
        navigationController?.popViewController(animated: true)
    }
    
    func saveUser(_ user: User){
        print("\nsaving user\n")
        var cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        var queryString: String?
        var methodNameValue = "PUT"
        var requestBody: String?
        var methodPath = "/user/" + user.id
        
        let headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
        
        if(methodNameValue == "PUT"){
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH:mm:ss.SSSZ"
            dateFormatter.locale = Locale(identifier: "en_GB")
            let calendar = Calendar.current
            let dobDate = calendar.date(from: user.dob!)
            let dobString = dateFormatter.string(from: dobDate!)
            print("\ndateString: \(dobString)")
            
            requestBody = "{\n  \"id\":\"\(user.id)\",\n  \"first_name\":\"\(user.first_name!)\",\n  \"last_name\":\"\(user.last_name!)\",\n  \"username\":\"\(user.username!)\",\n \"email\":\"\(user.email!)\",\n  \"bio\":\"\(user.bio!)\",\n  \"dob\":\"\(dobString)\" \n}"
            
            print("\(requestBody!)")
        }
        
        var parameters: [String: AnyObject]?
        var queryParameters = [String: String]()
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString != nil && queryString != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "" && requestBody != nil) {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            //self.txtTitle.text = "json not well formed"
            
            print("json error: \(error.localizedDescription)")
            return
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue, urlString: methodPath, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.txtTitle.text = "api error"
                    print("\napi error\n")
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            DispatchQueue.main.async {
                //strongSelf.txtBio.text = responseString
                User.currentUser = user
                User.currentUser.id = user.id
                User.justLoggedOut = true //
                
            }
            
            return nil
        })
    }
    

}

extension UIViewController{
    
    func setHideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard(){
        view.endEditing(true)
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}
