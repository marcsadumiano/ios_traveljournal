//
//  ChangePasswordViewController.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 12/05/2018.
//

import UIKit
import AWSUserPoolsSignIn

class ChangePasswordViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtNewPassword2: UITextField!
    @IBOutlet weak var btnChangePasswordOutlet: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnChangePasswordOutlet.layer.cornerRadius = 5
        
    }

    @IBAction func btnChangePassword(_ sender: UIButton) {
        if txtOldPassword.text! != "" && txtNewPassword.text! != "" && txtNewPassword.text! == txtNewPassword2.text! {
            changePassword()
        }
    }
    
    func changePassword(){
        
        //let user = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().getUser(User.currentUser.username!)
        let user = AWSCognitoUserPoolsSignInProvider.sharedInstance().getUserPool().currentUser()
        if let user = user{
            user.changePassword(txtOldPassword.text!, proposedPassword: txtNewPassword.text!).continueWith(block:{(task:AWSTask<AWSCognitoIdentityUserChangePasswordResponse>) -> Any? in
                
                if task.isCompleted && task.isFaulted != true {
                    
                    let alert = UIAlertController(title: "Save Password", message: "Password has been saved!", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action) in
                        
                        self.navigationController?.popViewController(animated: true)
                        
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                } else {
                    let alert = SimpleAlert.builder(title: "Error Saving", message: "Password does not match!", dismissTitle: "Try Again")
                    self.present(alert, animated: true, completion: nil)
                }
                
                return nil
            })
        } else{
            SimpleAlert.builder(title: "Error", message: "There was an error loading your account. Log in again", dismissTitle: "OK")
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
