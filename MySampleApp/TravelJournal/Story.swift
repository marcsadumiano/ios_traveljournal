//
//  Story.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 22/03/2018.
//

import Foundation

class Story{
    
    private var _id:String!
    private var _title:String!
    private var _date:DateComponents?
    private var _description:String!
    private var _photos = [Photo]()
    var index:Int?
    
    init(){
        _id = ""
        _title = ""
        _description = ""
    }
    
    init(id:String, title:String, description:String){
        self._id = id
        self._title = title
        self._description = description
    }
    
    var id:String{
        get{
            return _id!
        }
        set{
            self._id = newValue
        }
    }
    
    var title:String{
        get{
            return _title!
        }
        set{
            _title = newValue
        }
    }
    
    var date:DateComponents?{
        get{
            if _date != nil {
                return _date
            } else {
                return nil
            }
        }
        set(newVal){
            self._date = newVal
        }
    }
    
    var description:String{
        get{
            return _description!
        }
        set{
            _description = newValue
        }
    }
    
    var photos:[Photo]{
        get{
            return _photos
        }
        set{
            _photos = newValue
        }
    }
    
    func addPhoto(img:Photo){
        _photos.append(img)
    }
    
    func removePhoto(index:Int){
        _photos.remove(at: index)
    }
    
    func getDate(style:DateFormatter.Style) -> String{
        let formatter = DateFormatter()
        formatter.dateStyle = style
        formatter.timeStyle = .none
        
        let calendar = Calendar.current
        let date = calendar.date(from: _date!)
        
        let dateString = formatter.string(from: date!)
        return (dateString)
    }
    
}
