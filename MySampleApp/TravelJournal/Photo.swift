//
//  Photo.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 22/03/2018.
//

import Foundation

class Photo{
    
    private var _id:String?
    private var _filename:String?
    private var _photo:UIImage?
    var index: Int?
    
    
    init(_ image: UIImage){
        _photo = image
    }
    
    init(id:String, filename:String){
        self._id = id
        self._filename = filename
    }
    
    var id:String?{
        get{
            return _id
        }
        set{
            self._id = newValue
        }
    }
    
    var filename:String?{
        get{
            return _filename
        }
    }
    
    var photo:UIImage?{
        get{
            return self._photo
        }set{
            self._photo = newValue
        }
    }
    
}
