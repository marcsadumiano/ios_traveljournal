//
//  User.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 21/03/2018.
//

import Foundation

class User{
    
    static var currentUser = User()
    static var justLoggedOut:Bool = false
    static var mySections = [Section]()
    
    var profile_pic:UIImage?
    private var _id:String?
    private var _first_name:String?
    private var _last_name:String?
    private var _username:String?
    private var _dob:DateComponents?
    private var _email:String?
    private var _phone:String?
    private var _bio:String?
    private var _linkedAccounts:[String]?
    private var _journals:[Journal]?
    
    init(){}
    
    init(_ id:String){
        _id = id
    }
    
    init(id:String, first_name:String, last_name:String, username:String){
        self._id = id
        self._first_name = first_name
        self._last_name = last_name
        self._username = username
    }
    
    init(id:String, first_name:String, last_name:String, dob:DateComponents, username:String, email:String){
        _id = id
        _first_name = first_name
        _last_name = last_name
        _dob = dob
        _username = username
        _email = email
    }
    
    var id: String{
        get {
            return _id!
        }
        set{
            _id = newValue
        }
    }
    
    var first_name: String? {
        get {
            return _first_name
        }
        set{
            _first_name = newValue
        }
    }
    
    var last_name: String? {
        get{
            return _last_name
        }
        set{
            _last_name = newValue
        }
    }
    
    var dob: DateComponents? {
        get{
            return _dob
        }
        set{
            _dob = newValue
        }
    }
    
    var username: String? {
        get{
            return _username
        }
        set{
            _username = newValue
        }
    }
    
    var email: String? {
        get{
            return _email
        }
        set{
            _email = newValue
        }
    }
    
    var phone: String?{
        get{
            return _phone
        }
        set{
            _phone = newValue
        }
    }
    
    var bio: String?{
        get{
            return _bio
        }
        set{
            self._bio = newValue
        }
    }
    
    func getDOB(style:DateFormatter.Style) -> String?{
        let formatter = DateFormatter()
        formatter.dateStyle = style
        formatter.timeStyle = .none
        let calendar = Calendar.current
        
        if let dob = _dob{
            let date = calendar.date(from: dob)
            let dateString = formatter.string(from: date!)
            return (dateString)
        } else {
            return nil
        }
    }
    
}
