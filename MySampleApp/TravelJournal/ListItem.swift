//
//  ListItem.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 27/03/2018.
//

import Foundation

class ListItem{
    
    private var _id:String?
    private var _item:String!
    private var _isDone:Bool?
    
    init(item:String){
        self._item = item
        self._isDone = false
    }
    
    init(id:String, item:String){
        self._id = id
        self._item = item
        self._isDone = false
    }
    
    var id:String?{
        get{
            return self._id
        }
        set{
            self._id = newValue
        }
    }
    
    var item:String{
        get{
            return self._item
        }
        set(newVal){
            self._item = newVal
        }
    }
    
    var isDone:Bool?{
        get{
            return self._isDone
        }
        
        set(newVal){
            self._isDone = newVal
        }
    }
    
}

