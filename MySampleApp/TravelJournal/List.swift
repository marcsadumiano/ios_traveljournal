//
//  List.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 22/03/2018.
//

import Foundation

class List{
    
    private var _id:String?
    private var _title:String?
    private var _listItems = [ListItem]()
    var index:Int?
    
    init(){
        
    }
    
    init(id:String, title:String){
        self._id = id
        self._title = title
    }
    
    var id:String?{
        get{
            return _id
        }
        set{
            _id = newValue
        }
    }
    
    var title: String?{
        get{
            return _title
        }
        set{
            _title = newValue
        }
    }
    
    var listItems:[ListItem]{
        get{
            return _listItems
        }
        set{
            self._listItems = newValue
        }
    }
    
    func addListItem(item:ListItem){
        _listItems.append(item)
    }
    
    func removeListItem(index:Int){
        _listItems.remove(at: index)
    }
    
}
