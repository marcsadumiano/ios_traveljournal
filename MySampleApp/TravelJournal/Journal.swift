//
//  Journal.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 10/03/2018.
//

import Foundation

class Journal {
    
    private var _id:String?
    private var _title:String?
    private var _description:String?
    private var _start_date:DateComponents?
    private var _end_date:DateComponents?
    private var _country:String?
    private var _isPublic:Bool?
    private var _stories = [Story]()
    private var _lists = [List]()
    private var _user:User?
    
    init(user: User){
        self._user = user
        
    }
    
    init(id: String, title: String, description:String, start_date: DateComponents, end_date: DateComponents, country: String, isPublic: Bool, user:User){
        self._id = id
        self._description = description
        self._title = title
        self._start_date = start_date
        self._end_date = end_date
        self._country = country
        self._isPublic = isPublic
        self._user = user
        
    }
    
    var lists:[List]{
        get{
            return _lists
        }
        set(newVal){
            _lists = newVal
        }
    }
    
    var stories:[Story]{
        get{
            return _stories
        }
        set(newVal){
            _stories = newVal
        }
    }
    
    var user: User {
        get{
            return _user!
        }
    }
    
    var id: String? {
        get{
            return _id
        }
        set{
            self._id = newValue
        }
    }
    
    var title: String?{
        get{
            return _title
        }
        set(newTitle){
            self._title = newTitle
        }
    }
    
    var description: String?{
        get{
            return _description
        }
        set(newTitle){
            self._description = newTitle
        }
    }
    
    var country: String?{
        get{
            return _country
        }
        set(newCountry){
            self._country = newCountry
        }
    }
    
    var isPublic: Bool? {
        get{
            return _isPublic
        }
        set{
            _isPublic = newValue
        }
    }
    
    var start_date: DateComponents?{
        get{
            return (_start_date)
        }
        set{
            _start_date = newValue
        }
    }
    
    var end_date: DateComponents?{
        get{
            return (_end_date)
        }
        set{
            _end_date = newValue
        }
    }
    
    func getStartDate(style:DateFormatter.Style) -> String?{
        let formatter = DateFormatter()
        formatter.dateStyle = style
        formatter.timeStyle = .none
        let calendar = Calendar.current
        
        if let start_date = _start_date{
            let date = calendar.date(from: start_date)
            let dateString = formatter.string(from: date!)
            return (dateString)
        } else {
            return nil
        }
    }
    
    func getEndDate(style:DateFormatter.Style) -> String?{
        let formatter = DateFormatter()
        formatter.dateStyle = style
        formatter.timeStyle = .none
        let calendar = Calendar.current
        
        if let end_date = _end_date {
            let date = calendar.date(from: end_date)
            let dateString = formatter.string(from: date!)
            return (dateString)
        } else {
            return nil
        }
    }
    
}
