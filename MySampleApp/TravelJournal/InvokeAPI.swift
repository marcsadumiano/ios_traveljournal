//
//  InvokeAPI.swift
//  MySampleApp
//
//  Created by Marc Gil Sadumiano on 15/03/2018.
//

import Foundation
import AWSAuthCore
import AWSAPIGateway

class InvokeAPI {
    
    var cloudLogicAPI: CloudLogicAPI?
    let headerParameters = [
        "Content-Type": "application/json",
        "Accept": "application/json"
    ]
    
    var caller: CreateJournalViewController?
    
    var parameters: [String: AnyObject]?
    var queryParameters = [String: String]()
    var queryString: String?
    var methodNameValue: String?
    var requestBody: String?
    var methodPath: String?
    
    init(){
        queryString = ""
        requestBody = ""
        cloudLogicAPI = CloudLogicAPIFactory.cloudLogicAPI
        methodPath = "/journal"
        methodNameValue = HTTPMethodGet
        
    }
    
    func invoke(){
        
        // Parse query string parameters to Dictionary if not empty
        if (queryString! != "") {
            // check if the query string begins with a `?`
            if queryString!.hasPrefix("?") {
                // remove first character, i.e. `?`
                queryString!.remove(at: queryString!.startIndex)
                let keyValuePairStringArray = queryString!.components(separatedBy: "&")
                // check if there are any key value pairs
                if keyValuePairStringArray.count > 0 {
                    for pairString in keyValuePairStringArray {
                        let keyValue = pairString.components(separatedBy: "=")
                        if keyValue.count == 2 {
                            queryParameters.updateValue(keyValue[1], forKey: keyValue[0])
                        } else if keyValue.count == 1 {
                            queryParameters.updateValue("", forKey: keyValue[0])
                        } else {
                            print("Discarding query string for request: query string malformed.")
                        }
                    }
                }
            } else {
                print("Discarding query string for request: query string must begin with a `?`.")
            }
        }
        
        do {
            // Parse HTTP Body for methods apart from GET / HEAD
            if (methodNameValue != HTTPMethodGet &&
                methodNameValue != HTTPMethodHead) {
        
                // Parse the HTTP Body to JSON if not empty
                if (requestBody != "") {
                    let jsonInput = requestBody!.makeJsonable()
                    let jsonData = jsonInput.data(using: String.Encoding.utf8)!
                    parameters = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String: AnyObject]
                }
            }
        } catch let error as NSError {
            /*
            self.apiResponse.text = "JSON request is not well-formed."
            self.statusLabel.text = ""
            self.responseTimeLabel.text = ""
            */
            caller!.txtDescription.text = "error"
            print("json error: \(error.localizedDescription)")
            return
        }
        
        let apiRequest = AWSAPIGatewayRequest(httpMethod: methodNameValue!, urlString: methodPath!, queryParameters: queryParameters, headerParameters: headerParameters, httpBody: parameters)
        let startTime = Date()
        
        cloudLogicAPI?.apiClient?.invoke(apiRequest).continueWith(block: {[weak self](task: AWSTask) -> AnyObject? in
            guard let strongSelf = self?.caller! else { return nil }
            let endTime = Date()
            let timeInterval = endTime.timeIntervalSince(startTime)
            
            if let error = task.error {
                print("Error occurred: \(error)")
                
                DispatchQueue.main.async {
                    //strongSelf.apiResponse.text = "Error occurred while trying to invoke API: \(error)"
                    //strongSelf.statusLabel.text = ""
                    //strongSelf.responseTimeLabel.text = ""
                    strongSelf.txtDescription.text = "error occured while tryingf to invoke API"
                }
                return nil
            }
            
            let result: AWSAPIGatewayResponse! = task.result
            let responseString = String(data: result.responseData!, encoding: String.Encoding.utf8)
            
            print(responseString!)
            print(result.statusCode)
            
            
            DispatchQueue.main.async {
                //strongSelf.statusLabel.text = "\(result.statusCode)"
                //strongSelf.apiResponse.text = responseString
                //strongSelf.responseTimeLabel.text = String(format:"%.3f s", timeInterval)
                strongSelf.txtDescription.text = "\(result.statusCode)..."
            }
            strongSelf.txtTitle.text = "\(result.statusCode)"
            
            return nil
        })
        
    }
    
}

extension String {
    fileprivate func makeJsonable() -> String {
        let resultComponents: [String] = self.components(separatedBy: CharacterSet.newlines)
        return resultComponents.joined()
    }
}
